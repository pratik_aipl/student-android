package com.childrens.academy.testsection;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.childrens.academy.FullScreenActivity;
import com.childrens.academy.R;
import com.childrens.academy.adapter.OptionAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.QuestionsWebView;
import com.childrens.academy.view.xvideoplayer.QuestionAction;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class VideoQuestionActivity extends ActivityBase {

    @BindView(R.id.mQuestionText)
    QuestionsWebView mQuestionText;
    @BindView(R.id.mOptionList)
    RecyclerView mOptionList;
    @BindView(R.id.mRewatch)
    Button mRewatch;
    @BindView(R.id.mContinue)
    Button mContinue;
    @BindView(R.id.mViewContainer)
    LinearLayout mViewContainer;
    Unbinder unbinder;

    OptionAdapter optionAdapter;
    QuestionsBean questionsBean;
    int selPosion;
    List<OptionBean> optionBeans = new ArrayList<>();
    @BindView(R.id.mQuestionView)
    RelativeLayout mQuestionView;
    @BindView(R.id.mButtonView)
    LinearLayout mButtonView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_question_page);
        questionsBean = (QuestionsBean) getIntent().getSerializableExtra(Constant.selQuestion);
        selPosion = getIntent().getIntExtra(Constant.selPosion, 0);

        ButterKnife.bind(this);
        mQuestionText.loadHtmlFromLocal(questionsBean.getQuestion());
        mQuestionText.setInitialScale(30);

        mQuestionText.setBackgroundColor(Color.TRANSPARENT);
        mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mQuestionText.setScrollbarFadingEnabled(false);
        mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);


        mQuestionText.addJavascriptInterface(new WebInterface(this), "Android");

        optionBeans.clear();
        optionBeans.addAll(questionsBean.getOptions());
        mOptionList.setLayoutManager(new LinearLayoutManager(this));
        optionAdapter = new OptionAdapter(this, optionBeans);
        mOptionList.setAdapter(optionAdapter);
    }

    @OnClick({R.id.mRewatch, R.id.mContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mRewatch:
                callRewatch();
                break;
            case R.id.mContinue:
                callContinue();
                break;
        }
    }

    private void callRewatch() {
        onBackPressed();
    }

    private void callContinue() {
        boolean isCheck = false;
        for (int j = 0; j < optionBeans.size(); j++) {
            OptionBean optionBean = optionBeans.get(j);
            if (optionBean.isSelected()) {
                isCheck = true;
                questionsBean.setAnswerd(true);
                break;
            }
        }
        if (isCheck) {
            EventBus.getDefault().post(new QuestionAction(questionsBean, selPosion, false));
           onBackPressed();
        } else {
            Toast.makeText(this, "Please select answer.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new QuestionAction(null, selPosion, false));
        super.onBackPressed();
    }

    public class WebInterface {
        Context mContext;

        public WebInterface(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String toast) {
            startActivity(new Intent(mContext, FullScreenActivity.class).putExtra(Constant.imagePath, toast));
        }
    }


}
