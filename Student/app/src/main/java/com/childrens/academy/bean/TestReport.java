package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class TestReport implements Serializable {

    @JsonField(name = "test_details")
    TestDetails testDetails;

    @JsonField(name = "level_questions")
    List<LevelBean> levelQuestions;


    public TestDetails getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(TestDetails testDetails) {
        this.testDetails = testDetails;
    }

    public List<LevelBean> getLevelQuestions() {
        return levelQuestions;
    }

    public void setLevelQuestions(List<LevelBean> levelQuestions) {
        this.levelQuestions = levelQuestions;
    }
}
