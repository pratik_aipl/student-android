package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.assessment.Assessment;
import com.childrens.academy.assessment.Library;
import com.childrens.academy.assessment.LibraryChapter;
import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.ChapterOnClick;
import com.childrens.academy.listner.LibraryOnClick;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.testsection.CCT;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class LibraryChapterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "CCTAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<ChapterBean> paperBeans;
    ChapterOnClick testOnClick;

    public LibraryChapterAdapter(LibraryChapter cct, List<ChapterBean> paperBeans) {
        this.context = cct;
        this.paperBeans = paperBeans;
        this.testOnClick = cct;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_cct_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        ChapterBean paperBean = paperBeans.get(position);

        holder.mSubIcon.setVisibility(View.VISIBLE);
        holder.mDateView.setVisibility(View.GONE);

        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == paperBeans.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mViewContainer.setOnClickListener(v -> testOnClick.callbackChapter(position, paperBean));
        holder.mPaperName.setText(paperBean.getChapterName());
//        if (!TextUtils.isEmpty(paperBean.getPublishDate()) && paperBean.getPublishDate() != "null")
//            holder.mExpireDate.setText(context.getString(R.string.submition_on, Utils.changeDate(paperBean.getPublishDate())));
//        else
        holder.mExpireDate.setVisibility(View.GONE);


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mDay)
        TextView mDay;
        @BindView(R.id.mMonth)
        TextView mMonth;
        @BindView(R.id.mPaperName)
        TextView mPaperName;
        @BindView(R.id.mExpireDate)
        TextView mExpireDate;
        @BindView(R.id.mNextArrow)
        ImageView mNextArrow;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;
        @BindView(R.id.mSubIcon)
        ImageView mSubIcon;
        @BindView(R.id.mDateView)
        LinearLayout mDateView;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
