package com.childrens.academy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.AnswerBean;
import com.childrens.academy.view.OptionsWebView;
import com.childrens.academy.view.QuestionsWebView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryCorrectAnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AnswerBean> answerBeans;
    String page;

    public SummaryCorrectAnswerAdapter(Context context, List<AnswerBean> answerBeans, String page) {
        this.context = context;
        this.answerBeans = answerBeans;
        this.page=page;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_summary_answer_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;

        holder.mQueNo.setText((position + 1) + ")");
        holder.mQuestionText.loadHtmlFromLocal(answerBeans.get(position).getQuestion());
        holder.mQuestionText.setInitialScale(30);
        holder.mQuestionText.setBackgroundColor(Color.TRANSPARENT);
         holder.mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
         holder.mQuestionText.setScrollbarFadingEnabled(false);
         holder.mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        holder.mOptionList.setLayoutManager(new LinearLayoutManager(context));
        SummaryOptionAdapter optionAdapter = new SummaryOptionAdapter(context,answerBeans.get(position), answerBeans.get(position).getOptionBeans(),page);
        holder.mOptionList.setAdapter(optionAdapter);

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return answerBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mQueNo)
        public TextView mQueNo;
        @BindView(R.id.mQuestionText)
        public OptionsWebView mQuestionText;
        @BindView(R.id.mOptionList)
        public RecyclerView mOptionList;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
