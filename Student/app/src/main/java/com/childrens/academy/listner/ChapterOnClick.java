package com.childrens.academy.listner;


import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.LibraryBean;

public interface ChapterOnClick {
    public void callbackChapter(int pos, ChapterBean paperBean);
}
