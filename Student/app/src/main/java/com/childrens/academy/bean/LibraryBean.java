package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class LibraryBean implements Serializable {


    @JsonField(name = "SubjectID")
    int SubjectID;

    @JsonField(name = "SubjectTotalAssessment")
    int SubjectTotalAssessment;

    @JsonField(name = "SubjectName")
    String SubjectName;

    @JsonField(name = "Chapter_Info")
    List<ChapterBean> chapterList;

    public int getSubjectTotalAssessment() {
        return SubjectTotalAssessment;
    }

    public void setSubjectTotalAssessment(int subjectTotalAssessment) {
        SubjectTotalAssessment = subjectTotalAssessment;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public List<ChapterBean> getChapterList() {
        return chapterList;
    }

    public void setChapterList(List<ChapterBean> chapterList) {
        this.chapterList = chapterList;
    }
}
