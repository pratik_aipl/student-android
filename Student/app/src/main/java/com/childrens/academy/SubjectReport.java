package com.childrens.academy;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.adapter.ReportSubjectDetailsAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.bean.SubjectSummaryDetails;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.listner.ReportDetailsSubjectOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.testsection.TodayTestReport;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.NonScrollRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SubjectReport extends ActivityBase implements ReportDetailsSubjectOnClick, NetworkStateReceiver.NetworkStateReceiverListener {


    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;

    @BindView(R.id.mAssignmentList)
    NonScrollRecyclerView mAssignmentList;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;
    ReportSubjectDetailsAdapter reportSubjectDetailsAdapter;

//    List<ReportSubjectDetailsAdapter> searchBeans = new ArrayList<>();
    Subscription subscriptionSearchPaperList;

    @BindView(R.id.mPerformance)
    TextView mPerformance;
    @BindView(R.id.mCompleted)
    TextView mCompleted;
    @BindView(R.id.mMissed)
    TextView mMissed;
    @BindView(R.id.mCount)
    TextView mCount;


    private static final String TAG = "SubjectReport";

    SubjectBean subjectReport;
    SubjectSummaryDetails subjectSummaryDetails;
    public NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        subjectReport= (SubjectBean) getIntent().getSerializableExtra(Constant.subjectReport);
        mPageTitle.setText(subjectReport.getSubjectName());



        mPageTitle.setText(subjectReport.getSubjectName());
        mPerformance.setText(subjectReport.getOverallPerformance()+"%");
        mCount.setText(getString(R.string.total_20, (subjectReport.getTotalAttempt()) + subjectReport.getTotalMissed()));
        mCompleted.setText(subjectReport.getTotalAttempt() + "");
        mMissed.setText(subjectReport.getTotalMissed() + "");

    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }
    @Override
    public void callbackPaper(int pos, PaperBean searchBean) {
        if (Utils.isNetworkAvailable(this, true))
            startActivity(new Intent(this, TodayTestReport.class).putExtra(Constant.StudentMCQTestID, searchBean.getStudentMCQTestID()));
    }

    @Override
    protected void onDestroy() {
        if (subscriptionSearchPaperList != null && !subscriptionSearchPaperList.isUnsubscribed()) {
            subscriptionSearchPaperList.unsubscribe();
            subscriptionSearchPaperList = null;
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {
        //Subject_student_planner
        SubjectStudentPlanner(true, subjectReport.getSubjectID());

    }

    @Override
    public void networkUnavailable() {
        if (!Constant.isAlertShow) {
            Constant.isAlertShow = true;
            Utils.showTwoButtonDialog(this, getString(R.string.network_error_title), getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                @Override
                public void onPositiveButtonClicked() {
                    Constant.isAlertShow = false;
                }

                @Override
                public void onNegativButtonClicked() {
                }
            });
        }
    }
    private void SubjectStudentPlanner(boolean isShow, String subjectIds) {
        Map<String, String> map = new HashMap<>();
            map.put(Constant.SubjectID, subjectIds);

        showProgress(isShow);
        subscriptionSearchPaperList = NetworkRequest.performAsyncRequest(restApi.getSubjectStudentPlanner(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "SubjectStudentPlanner: "+data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());
                    subjectSummaryDetails = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), SubjectSummaryDetails.class);
                    setData();
//                    searchBeans.clear();
//                    searchBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), SearchBean.class));
//                    reportSubjectDetailsAdapter.notifyDataSetChanged();
//                    if (searchBeans.size() > 0) {
//                        mAssignmentList.setVisibility(View.VISIBLE);
//                        mEmptyView.setVisibility(View.GONE);
//                    } else {
//                        mAssignmentList.setVisibility(View.GONE);
//                        mEmptyView.setVisibility(View.VISIBLE);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData() {


        List<PaperBean> list=new ArrayList<>();

        list.addAll(subjectSummaryDetails.getPaper_info());
        list.addAll(subjectSummaryDetails.getMissed_Paper_info());

        reportSubjectDetailsAdapter = new ReportSubjectDetailsAdapter(this, list);
        mAssignmentList.setAdapter(reportSubjectDetailsAdapter);

        if (list.size() > 0) {
            mAssignmentList.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mAssignmentList.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }
}
