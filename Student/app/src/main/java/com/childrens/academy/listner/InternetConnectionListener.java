package com.childrens.academy.listner;

public interface InternetConnectionListener {
    void onInternetUnavailable();

    void onCacheUnavailable();
}
