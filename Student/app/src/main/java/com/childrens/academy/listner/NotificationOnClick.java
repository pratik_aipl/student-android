package com.childrens.academy.listner;


import com.childrens.academy.bean.NotificationBean;

public interface NotificationOnClick {
    public void callNotificationClick(int pos, NotificationBean notificationBean);
}
