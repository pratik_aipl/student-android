package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class TestSummaryDetails implements Serializable {

    @JsonField
    int OverallPerformance;

    @JsonField
    int TotalAttempt;

    @JsonField
    int TotalAssign;

    @JsonField
    int TotalMissed;

    @JsonField
    List<SubjectBean> Subject;

    public int getOverallPerformance() {
        return OverallPerformance;
    }

    public void setOverallPerformance(int overallPerformance) {
        OverallPerformance = overallPerformance;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getTotalAssign() {
        return TotalAssign;
    }

    public void setTotalAssign(int totalAssign) {
        TotalAssign = totalAssign;
    }

    public int getTotalMissed() {
        return TotalMissed;
    }

    public void setTotalMissed(int totalMissed) {
        TotalMissed = totalMissed;
    }

    public List<SubjectBean> getSubject() {
        return Subject;
    }

    public void setSubject(List<SubjectBean> subject) {
        Subject = subject;
    }
}
