package com.childrens.academy;

import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.adapter.LegalAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.LegalData;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Legal extends ActivityBase implements NetworkStateReceiver.NetworkStateReceiverListener {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    boolean isAlertShow = false;

    Subscription subscriptionLegal;
    @BindView(R.id.mRecyclerView)
    RecyclerView mLegalList;

    public NetworkStateReceiver networkStateReceiver;
    private RecyclerView.LayoutManager layoutManager;
    private LegalAdapter legalAdapter;
    List<LegalData> legalList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);
        ButterKnife.bind(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        mPageTitle.setText("Legal");


        layoutManager = new LinearLayoutManager(this);
        mLegalList.setLayoutManager(layoutManager);
        mLegalList.setItemAnimator(new DefaultItemAnimator());

        legalAdapter = new LegalAdapter(this, legalList);
        mLegalList.setAdapter(legalAdapter);
    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }


    private void getLegalData(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionLegal = NetworkRequest.performAsyncRequest(restApi.getLegal(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    legalList.clear();
                    legalList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), LegalData.class));
                    legalAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @Override
    protected void onDestroy() {
        if (subscriptionLegal != null && !subscriptionLegal.isUnsubscribed()) {
            subscriptionLegal.isUnsubscribed();
            subscriptionLegal = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        super.onDestroy();
    }

    @Override
    public void networkAvailable() {
        getLegalData(true);
    }

    @Override
    public void networkUnavailable() {
        if (!isAlertShow) {
            isAlertShow = true;
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getString(R.string.network_error_title));
            alertDialogBuilder
                    .setMessage(getString(R.string.network_connection_error));
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isAlertShow = false;
                }
            });
            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    }
}
