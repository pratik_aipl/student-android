package com.childrens.academy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.childrens.academy.R;
import com.childrens.academy.adapter.viewholder.SubChapterViewHolder;
import com.childrens.academy.adapter.viewholder.ChapterViewHolder;
import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.expan.Artist;
import com.childrens.academy.bean.expan.Genre;
import com.childrens.academy.view.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.childrens.academy.view.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Peep on 04/12/18.
 */
public class ChapterAdapter extends ExpandableRecyclerViewAdapter<ChapterViewHolder, SubChapterViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;

    public ChapterAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public ChapterViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_chapter_item, parent, false);
        return new ChapterViewHolder(view);
    }

    @Override
    public SubChapterViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_sub_chapter_item, parent, false);
        return new SubChapterViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SubChapterViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

        final Artist artist = ((Genre) group).getItems().get(childIndex);
        holder.setArtistName("* "+artist.getName());
    }

    @Override
    public void onBindGroupViewHolder(ChapterViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {

        holder.setGenreTitle(group);
    }


}
