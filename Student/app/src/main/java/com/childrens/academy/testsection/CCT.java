package com.childrens.academy.testsection;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.CCTAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.InternetConnetionLostEvent;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class CCT extends ActivityBase implements TestOnClick, NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = "CCT";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    Subscription subscriptionPaperList;
    CCTAdapter cctAdapter;
    List<PaperBean> paperBeans = new ArrayList<>();
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;
    boolean isAlertShow = false;
    public NetworkStateReceiver networkStateReceiver;
    android.support.v7.app.AlertDialog alertDialog;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_paper);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        mPageTitle.setText("ASSIGNMENT");
        mSearch.setVisibility(View.VISIBLE);
        mSearch.setImageResource(R.mipmap.filter);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        cctAdapter = new CCTAdapter(this, paperBeans);
        mRecyclerView.setAdapter(cctAdapter);
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Assignment Loading...");
        if (paperBeans.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.network_error_title));
        alertDialogBuilder.setMessage(getString(R.string.network_connection_error));
        alertDialogBuilder.setPositiveButton("OK", (dialog, which) -> isAlertShow = false);
        alertDialog = alertDialogBuilder.create();

    }


    private void getPapers(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.getPaperList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    paperBeans.clear();
                    paperBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), PaperBean.class));
                    cctAdapter.notifyDataSetChanged();
                    mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
                    tvEmpty.setText("No Assignment Pending");
                    if (paperBeans.size() > 0) {
                        if (snackbar != null && snackbar.isShown())
                            snackbar.dismiss();
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mEmptyView.setVisibility(View.GONE);
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                if (Utils.isNetworkAvailable(this, true))
                    startActivity(new Intent(this, SearchTestPaper.class));
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInternetConnetionLostEvent(InternetConnetionLostEvent event) {
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Network Connections Error!!");
        if (paperBeans.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        callSnackBar();
    }

    private void callSnackBar() {
        snackbar = Snackbar
                .make(mBackBtn, "Please check internet connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> {
                    if (Utils.isNetworkAvailable(CCT.this, false))
                        getPapers(true);
                    else
                        callSnackBar();
                });
        snackbar.show();
    }


    @Override
    public void callbackPeep(int pos, PaperBean paperBean) {
        Log.d(TAG, "callbackPeep: "+new Gson().toJson(paperBean));
        if (paperBean.getTotalQuestion() > 0) {
            if (Utils.isNetworkAvailable(this, false)) {
                startActivity(new Intent(this, TestInfo.class).putExtra(Constant.selPaper, paperBean));
            } else
                callSnackBar();
        }
    }

    @Override
    public void onDestroy() {
        if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    @Override
    public void networkAvailable() {
        getPapers(true);
    }

    @Override
    public void networkUnavailable() {
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Network Connections Error!!");
        if (paperBeans.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
//        if (alertDialog != null && !alertDialog.isShowing())
//            alertDialog.show();
    }
}
