package com.childrens.academy.listner;


import com.childrens.academy.bean.SubjectBean;

public interface TestDetailsSubjectOnClick {
    public void callbackPaper(int pos, SubjectBean searchBean);
}
