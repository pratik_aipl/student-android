package com.childrens.academy.testsection;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.BuildConfig;
import com.childrens.academy.R;
import com.childrens.academy.adapter.QuestionPagerAdaptor;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.AnswerDetailBean;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.CountDownTimerPausable;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.childrens.academy.utils.Utils.getDurationBreakdown;
import static com.childrens.academy.utils.Utils.strToMilli;

public class TodayTest extends ActivityBase implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = "TodayTest";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mLabel)
    TextView mRightText;
    @BindView(R.id.mTimer)
    TextView mTimer;
    @BindView(R.id.mPager)
    ViewPager mPager;
    QuestionPagerAdaptor questionPagerAdaptor;
    List<QuestionsBean> questionsBeans = new ArrayList<>();
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mNext)
    ImageView mNext;
    boolean isAutoEndTest = false;
    PaperBean paperBean;
    Subscription subscriptionQuestionsList, subscriptionSubmitTest;
    CountDownTimerPausable countDownTimerPausable;
    boolean isAlertShow = false;
    @BindView(R.id.mDisableView)
    View mDisableView;
    public NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.today_test);
        ButterKnife.bind(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.selPaper);
        mEndTest.setVisibility(View.VISIBLE);
        mPageTitle.setText(paperBean.getSubjectName());

        if (Utils.isNetworkAvailable(this, true))
            getQuestionsList(true);

        setQuestionPage();

    }


    private void getQuestionsList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, paperBean.getMCQPlannerID());
        showProgress(isShow);
        subscriptionQuestionsList = NetworkRequest.performAsyncRequest(restApi.getPaperQuestionsList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    questionsBeans.clear();
                    questionsBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), QuestionsBean.class));
                    questionPagerAdaptor.notifyDataSetChanged();
                    updateIndicatorTv();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setQuestionPage() {
        questionPagerAdaptor = new QuestionPagerAdaptor(getSupportFragmentManager(), questionsBeans);

        mPager.setAdapter(questionPagerAdaptor);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mPrev.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle));
                } else if (position == (mPager.getAdapter().getCount() - 1)) {
                    mNext.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle));
                } else {
                    mPrev.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle_primary));
                    mNext.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle_primary));
                }
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (questionsBeans.size() != 0)
            mPager.setOffscreenPageLimit((questionsBeans.size() - 1));
        updateIndicatorTv();
        if (!TextUtils.isEmpty(paperBean.getTime())) {
            countDownTimerPausable = new CountDownTimerPausable(strToMilli(paperBean.getTime()), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimer.setText("" + getDurationBreakdown(millisUntilFinished));
                }

                @Override
                public void onFinish() {
                    isAutoEndTest = true;
                    mDisableView.setVisibility(View.VISIBLE);
                    int attempt = 0, notAttept = 0, total = 0;
                    for (int i = 0; i < questionsBeans.size(); i++) {
                        for (int j = 0; j < questionsBeans.get(i).getOptions().size(); j++) {
                            OptionBean optionBean = questionsBeans.get(i).getOptions().get(j);
                            if (optionBean.isSelected()) {
                                attempt = attempt + 1;
                            } else {
                                notAttept = notAttept + 1;
                            }
                        }
                    }
                    total = attempt + notAttept;
                    Log.d(TAG, "onFinish: " + attempt + " " + notAttept + " " + total);

                    if (Utils.isNetworkAvailable(TodayTest.this, true)) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TodayTest.this);
                        alertDialogBuilder.setTitle("");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("<b>Your time should be complete.<br />Please click on OK to see your reports.<br /><br />Total Question - "+total+"<br /><b>Attempted&emsp;&emsp;- "+attempt+"<br />Not Attempted - "+notAttept+"</b>");
                        alertDialogBuilder.setPositiveButton("OK", (dialog, which) -> submitTest(true)).show();
                    }
                }
            };
        }
    }


    @Override
    protected void onPause() {
        if (countDownTimerPausable != null && !countDownTimerPausable.isPaused())
            countDownTimerPausable.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + isAlertShow);
        if (countDownTimerPausable != null && countDownTimerPausable.isPaused())
            if (!isAlertShow)
                countDownTimerPausable.start();
    }


    private void updateIndicatorTv() {
        int totalNum = mPager.getAdapter().getCount();
        int currentItem = mPager.getCurrentItem() + 1;
        mRightText.setText(currentItem + " / " + totalNum);
    }


    @OnClick({R.id.mPrev, R.id.mNext, R.id.mBackBtn, R.id.mEndTest, R.id.mDisableView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mDisableView:
                break;
            case R.id.mEndTest:
                if (Utils.isNetworkAvailable(TodayTest.this, true)) {
                    isAlertShow = true;
                    if (countDownTimerPausable != null && !countDownTimerPausable.isPaused())
                        countDownTimerPausable.pause();
                    int attempt=0,notAttept=0,total=0;
                    List<AnswerDetailBean> answerDetailBeans = new ArrayList<>();
                    for (int i = 0; i < questionsBeans.size(); i++) {
                        OptionBean selectOption = new OptionBean();
                        for (int j = 0; j < questionsBeans.get(i).getOptions().size(); j++) {
                            OptionBean optionBean = questionsBeans.get(i).getOptions().get(j);
                            if (optionBean.isSelected()) {
                                selectOption.setMCQOptionID(optionBean.getMCQOptionID());
                                selectOption.setAttempt(optionBean.isAttempt());
                                break;
                            }
                            selectOption.setMCQOptionID(optionBean.getMCQOptionID());
                        }
                        answerDetailBeans.add(new AnswerDetailBean(questionsBeans.get(i).getMCQPlannerDetailsID(), selectOption.isAttempt() ? selectOption.getMCQOptionID() : 0, selectOption.isAttempt() ? "1" : "0"));
                    }
                    for (int i = 0; i < answerDetailBeans.size(); i++) {
                        Log.d(TAG, "onViewClicked: 0 --> "+answerDetailBeans.get(i).getIsAttempt());
                            if (answerDetailBeans.get(i).getIsAttempt().equalsIgnoreCase("1")) {
                                attempt = attempt + 1;
                            } else {
                                notAttept = notAttept + 1;
                            }
                    }
                    total = attempt + notAttept;
                    Log.d(TAG, "onFinish: " + attempt + " " + notAttept + " " + total);

                    Utils.showTwoButtonDialog(this, "", "<b>Do you really want to submit the test?<br /><br />Total Question - "+total+"<br />Attempted&emsp;&emsp;- "+attempt+"<br />Not Attempted - "+notAttept+"</b>", "Submit", "Cancel", new DialogButtonListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            submitTest(true);
                        }

                        @Override
                        public void onNegativButtonClicked() {
                            isAlertShow = false;
                            if (countDownTimerPausable != null && countDownTimerPausable.isPaused())
                                countDownTimerPausable.start();
                        }
                    });
                }
                break;
            case R.id.mPrev:
                if (mPager.getCurrentItem() > 0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
                break;
            case R.id.mNext:
                if (mPager.getCurrentItem() != mPager.getAdapter().getCount()) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (countDownTimerPausable != null && !countDownTimerPausable.isPaused())
            countDownTimerPausable.pause();
        Utils.showTwoButtonDialog(this, "Leave Test", "Do you really want to leave test?", "Yes", "No", new DialogButtonListener() {
            @Override
            public void onPositiveButtonClicked() {
                finish();
            }

            @Override
            public void onNegativButtonClicked() {
                isAlertShow = false;
                if (countDownTimerPausable != null && countDownTimerPausable.isPaused())
                    countDownTimerPausable.start();
            }
        });

    }

    private void submitTest(boolean isShow) {

        List<AnswerDetailBean> answerDetailBeans = new ArrayList<>();
//        List<AnswerDetailBean> answerDetailBeansSample = new ArrayList<>();
        for (int i = 0; i < questionsBeans.size(); i++) {
            OptionBean selectOption = new OptionBean();
            for (int j = 0; j < questionsBeans.get(i).getOptions().size(); j++) {
                OptionBean optionBean = questionsBeans.get(i).getOptions().get(j);
                if (optionBean.isSelected()) {
                    selectOption.setMCQOptionID(optionBean.getMCQOptionID());
                    selectOption.setAttempt(optionBean.isAttempt());
                    break;
                } else {
//                    Log.d(TAG, j+" => submitTest: " + optionBean.isSelected());
                }
                selectOption.setMCQOptionID(optionBean.getMCQOptionID());
            }
            answerDetailBeans.add(new AnswerDetailBean(questionsBeans.get(i).getMCQPlannerDetailsID(), selectOption.isAttempt() ? selectOption.getMCQOptionID() : 0, selectOption.isAttempt() ? "1" : "0"));
        }

        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, paperBean.getMCQPlannerID());
        map.put(Constant.detailArray, gson.toJson(answerDetailBeans));
        if (countDownTimerPausable != null) {
            long takenTime = strToMilli(paperBean.getTime()) - countDownTimerPausable.getMillisRemaining();
            map.put(Constant.TakenTime, String.valueOf(takenTime));
        }
        if (BuildConfig.DEBUG)
            Log.d(TAG, "submitTest: sample data " + gson.toJson(answerDetailBeans));
        showProgress(isShow);
        subscriptionSubmitTest = NetworkRequest.performAsyncRequest(restApi.submitTest(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                    if (jsonObject.has(Constant.StudentMCQTestID)) {
                        startActivity(new Intent(this, TodayTestReport.class)
                                .putExtra(Constant.isTest, true)
                                .putExtra(Constant.StudentMCQTestID, jsonObject.getString(Constant.StudentMCQTestID))
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    protected void onDestroy() {
        if (subscriptionQuestionsList != null && !subscriptionQuestionsList.isUnsubscribed()) {
            subscriptionQuestionsList.unsubscribe();
            subscriptionQuestionsList = null;
        }
        if (subscriptionSubmitTest != null && !subscriptionSubmitTest.isUnsubscribed()) {
            subscriptionSubmitTest.unsubscribe();
            subscriptionSubmitTest = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        super.onDestroy();
    }


    @Override
    public void networkAvailable() {
        if (isAutoEndTest)
            submitTest(true);
    }

    @Override
    public void networkUnavailable() {
//        Utils.showTwoButtonDialog(this, getString(R.string.network_error_title), getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//                Constant.isAlertShow = false;
//            }
//
//            @Override
//            public void onNegativButtonClicked() {
//            }
//        });
    }
}
