package com.childrens.academy.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.view.expandablerecyclerview.viewholders.ChildViewHolder;

public class SubChapterViewHolder extends ChildViewHolder {

  private TextView mSubChapter;

  public SubChapterViewHolder(View itemView) {
    super(itemView);
    mSubChapter = (TextView) itemView.findViewById(R.id.mChapterName);
  }

  public void setArtistName(String name) {
    mSubChapter.setText(name);
  }
}
