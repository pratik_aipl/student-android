package com.childrens.academy.assessment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.childrens.academy.R;
import com.childrens.academy.assessment.view.QuestionsWebView;
import com.childrens.academy.base.BaseFragment;
import com.childrens.academy.bean.LevelQuestion;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.listner.OptionsOnClick;
import com.childrens.academy.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.childrens.academy.utils.Utils.stringForTime;


public class QuestionPageFragment extends BaseFragment implements OptionsOnClick {

    @BindView(R.id.mQuestionPointTime)
    TextView mQuestionPointTime;
    @BindView(R.id.mQuestionText)
    QuestionsWebView mQuestionText;
    @BindView(R.id.mOptionList)
    RecyclerView mOptionList;
    @BindView(R.id.mViewContainer)
    LinearLayout mViewContainer;
    Unbinder unbinder;

    QuestionsBean questionsBean;
    List<OptionBean> optionBeans = new ArrayList<>();
    long startTime;
    // Store instance variables

    public static QuestionPageFragment newInstance(int i, QuestionsBean questionsBean, long startTime) {
        QuestionPageFragment fragmentFirst = new QuestionPageFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.selQuestion, questionsBean);
        args.putLong(Constant.startTime, startTime);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionsBean = (QuestionsBean) getArguments().getSerializable(Constant.selQuestion);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_page_assessment, container, false);
        unbinder = ButterKnife.bind(this, view);
        mQuestionText.loadHtmlFromLocal(questionsBean.getQuestion());
        startTime = getArguments().getLong(Constant.startTime);
        mQuestionText.setInitialScale(40);

        String time = stringForTime((questionsBean.getQuestionMarkers() - startTime));
        if (time.equalsIgnoreCase("00:00")) {
            mQuestionPointTime.setVisibility(View.GONE);
        } else {
            mQuestionPointTime.setText(time);
            mQuestionPointTime.setVisibility(View.VISIBLE);
        }

        mQuestionText.setBackgroundColor(Color.TRANSPARENT);
        mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mQuestionText.setScrollbarFadingEnabled(false);
        mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        optionBeans.clear();
        optionBeans.addAll(questionsBean.getOptions());
        mOptionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mOptionList.setAdapter(new OptionAdapter(QuestionPageFragment.this, optionBeans, false, 0, 0, false, 0));
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void callbackOption(int pos, OptionBean paperBean) {

        for (int i = 0; i < optionBeans.size(); i++) {
            optionBeans.get(i).setClickPos(i);
            optionBeans.get(pos).setAttempt(true);
            optionBeans.get(pos).setSelected(true);
        }
        mOptionList.setAdapter(new OptionAdapter(QuestionPageFragment.this, optionBeans, false, 0, 0, false, 0));
    }
}
