package com.childrens.academy.listner;


import com.childrens.academy.bean.LibraryBean;

public interface LibraryOnClick {
    public void callbackLibrary(int pos, LibraryBean paperBean);
}
