package com.childrens.academy.assessment;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.CCTAdapter;
import com.childrens.academy.adapter.LibraryChapterAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.LibraryBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.ChapterOnClick;
import com.childrens.academy.listner.InternetConnetionLostEvent;
import com.childrens.academy.listner.LibraryOnClick;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LibraryChapter extends ActivityBase implements ChapterOnClick {

    private static final String TAG = "LibraryChapter";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    Subscription subscriptionPaperList;
    LibraryChapterAdapter cctAdapter;
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;
    boolean isAlertShow = false;
    public NetworkStateReceiver networkStateReceiver;
    android.support.v7.app.AlertDialog alertDialog;
    Snackbar snackbar;

    LibraryBean libraryBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_paper);
        ButterKnife.bind(this);

        libraryBean = (LibraryBean) getIntent().getSerializableExtra(Constant.paper);
        mPageTitle.setText(libraryBean.getSubjectName());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Log.d(TAG, "onCreate: "+libraryBean.getChapterList().size());

        cctAdapter = new LibraryChapterAdapter(this, libraryBean.getChapterList());
        mRecyclerView.setAdapter(cctAdapter);
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Library Loading...");
        if (libraryBean.getChapterList().size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.network_error_title));
        alertDialogBuilder.setMessage(getString(R.string.network_connection_error));
        alertDialogBuilder.setPositiveButton("OK", (dialog, which) -> isAlertShow = false);
        alertDialog = alertDialogBuilder.create();

    }

    @OnClick({R.id.mBackBtn, R.id.mRightText, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                if (Utils.isNetworkAvailable(this, true))
                    startActivity(new Intent(this, SearchTestPaper.class));
                break;
            case R.id.mRightText:
                if (Utils.isNetworkAvailable(this, true))
                    startActivity(new Intent(this, SearchTestPaper.class));
                break;
        }
    }

    @Override
    public void onDestroy() {
        if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }



    @Override
    public void callbackChapter(int pos, ChapterBean paperBean) {
            startActivity(new Intent(this, LibraryPaperInfo.class)
                    .putExtra(Constant.paper, paperBean));
    }
}
