package com.childrens.academy;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class MobileScreen extends ActivityBase {

    private static final String TAG = "MobileScreen";
    @BindView(R.id.mLogoView)
    LinearLayout mLogoView;
    @BindView(R.id.mMobileNo)
    EditText mMobileNo;
    @BindView(R.id.mSubmit)
    Button mSubmit;
    private Subscription subscriptionLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_screen);
        ButterKnife.bind(this);
        if (BuildConfig.DEBUG) {
            mMobileNo.setText("PA0012");
            mMobileNo.setText("RMG-007");
        }

    }

    @OnClick(R.id.mSubmit)
    public void onViewClicked() {
        if (!TextUtils.isEmpty(mMobileNo.getText().toString().trim())) {
            if (Utils.isNetworkAvailable(this, true))
                callLogin();
        } else
            mMobileNo.setError("Please enter your UID number");
    }


    private void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.StudentCode, mMobileNo.getText().toString().trim().toUpperCase());
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        Log.d(TAG, "callLogin: " + map);
        showProgress(true);
        subscriptionLogin = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                    String userId = "", mobileno = "", Otp = "";
                    if (jsonObject.has(Constant.user_id)) {
                        userId = jsonObject.getString(Constant.user_id);
                    }
                    if (jsonObject.has(Constant.otp)) {
                        Otp = jsonObject.getString(Constant.otp);
                    }
                    if (jsonObject.has(Constant.mobileno)) {
                        mobileno = jsonObject.getString(Constant.mobileno);
                    }
                    Intent i = new Intent(this, VerificationScreen.class);
                    i.putExtra(Constant.StudentCode, mMobileNo.getText().toString().trim());
                    i.putExtra(Constant.MobileNo, mobileno);
                    i.putExtra(Constant.otp, Otp);
                    i.putExtra(Constant.message, jsonResponse.getString(Constant.message));
                    startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            showConnectionSnackBarUserLogin();
        });
    }

    @Override
    protected void onDestroy() {

        if (subscriptionLogin != null && !subscriptionLogin.isUnsubscribed()) {
            subscriptionLogin.unsubscribe();
            subscriptionLogin = null;
        }
        super.onDestroy();
    }
}