package com.childrens.academy.testsection;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.childrens.academy.FullScreenActivity;
import com.childrens.academy.R;
import com.childrens.academy.adapter.OptionAdapter;
import com.childrens.academy.base.BaseFragment;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.view.QuestionsWebView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QuestionPageFragment extends BaseFragment {

    private static final String TAG = "QuestionPageFragment";

    @BindView(R.id.mQuestionText)
    QuestionsWebView mQuestionText;
    @BindView(R.id.mOptionList)
    RecyclerView mOptionList;
    @BindView(R.id.mViewContainer)
    LinearLayout mViewContainer;
    Unbinder unbinder;

    OptionAdapter optionAdapter;
    QuestionsBean questionsBean;
    List<OptionBean> optionBeans = new ArrayList<>();

    // Store instance variables

    public static QuestionPageFragment newInstance(int i, QuestionsBean questionsBean) {
        QuestionPageFragment fragmentFirst = new QuestionPageFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.selQuestion, questionsBean);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionsBean = (QuestionsBean) getArguments().getSerializable(Constant.selQuestion);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_page_assessment, container, false);
        unbinder = ButterKnife.bind(this, view);
        mQuestionText.loadHtmlFromLocal(questionsBean.getQuestion());
        mQuestionText.setInitialScale(30);

        mQuestionText.setBackgroundColor(Color.TRANSPARENT);
        mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mQuestionText.setScrollbarFadingEnabled(false);
        mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);


        mQuestionText.addJavascriptInterface(new WebInterface(getActivity()), "Android");

        optionBeans.clear();
        optionBeans.addAll(questionsBean.getOptions());
        mOptionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        optionAdapter = new OptionAdapter(getActivity(), optionBeans);
        mOptionList.setAdapter(optionAdapter);
        return view;
    }

    public class WebInterface {
        Context mContext;
        public WebInterface(Context c) { this.mContext = c;}

        @JavascriptInterface
        public void showToast(String toast) {
            startActivity(new Intent(mContext, FullScreenActivity.class).putExtra(Constant.imagePath,toast));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
