package com.childrens.academy.testsection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.DashBoard;
import com.childrens.academy.R;
import com.childrens.academy.adapter.TestReportLevelAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.LevelBean;
import com.childrens.academy.bean.TestDetails;
import com.childrens.academy.bean.TestReport;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.childrens.academy.utils.Utils.getDurationBreakdown;

public class TodayTestReport extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mReportLevel)
    RecyclerView mReportLevel;
    @BindView(R.id.mTimeTaken)
    TextView mTimeTaken;
    @BindView(R.id.mAvgTime)
    TextView mAvgTime;
    TestReport testReport;
    Subscription subscriptionTestReport;
    @BindView(R.id.mSubjectName)
    TextView mSubjectName;
    @BindView(R.id.mCorrectAns)
    TextView mCorrectAns;
    @BindView(R.id.mTotalQuestion)
    TextView mTotalQuestion;
    @BindView(R.id.mCorrectAns1)
    TextView mCorrectAns1;
    @BindView(R.id.mTotalQuestion1)
    TextView mTotalQuestion1;
    @BindView(R.id.mInCorrectAns)
    TextView mInCorrectAns;
    @BindView(R.id.mTotalQuestion2)
    TextView mTotalQuestion2;
    @BindView(R.id.mNotAns)
    TextView mNotAns;
    @BindView(R.id.mTotalQuestion3)
    TextView mTotalQuestion3;
    @BindView(R.id.mGoldStaticAccuracy)
    ProgressBar mGoldStaticAccuracy;
    @BindView(R.id.mSubjectAccuracy)
    ProgressBar mSubjectAccuracy;
    @BindView(R.id.mSubjectAccuracyLabel)
    TextView mSubjectAccuracyLabel;

    TestReportLevelAdapter testReportLevelAdapter;
    List<LevelBean> testLevelBeans = new ArrayList<>();
    //    PaperBean testDetails;
    String studentMCQTestID;
    boolean isTest;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.today_test_report);
        ButterKnife.bind(this);

        isTest = (boolean) getIntent().getBooleanExtra(Constant.isTest, false);
        studentMCQTestID = getIntent().getStringExtra(Constant.StudentMCQTestID);

        mPageTitle.setText("Test Report");


        mRightText.setVisibility(View.VISIBLE);
        mRightText.setText("Summary");

        if (Utils.isNetworkAvailable(this, true))
            testReports(true);
    }


    @OnClick({R.id.mBackBtn, R.id.mRightText})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mRightText:
                if (Utils.isNetworkAvailable(this, true))
                    startActivity(new Intent(this, TodayTestSummary.class).putExtra(Constant.StudentMCQTestID, studentMCQTestID));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isTest) {
            startActivity(new Intent(this, DashBoard.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        } else {
            super.onBackPressed();
        }

    }

    private void testReports(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, studentMCQTestID);
        showProgress(isShow);
        subscriptionTestReport = NetworkRequest.performAsyncRequest(restApi.testReport(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    testReport = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), TestReport.class);
                    setData(testReport);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData(TestReport testReport) {
        TestDetails testDetails = testReport.getTestDetails();
        mSubjectName.setText(testDetails.getSubjectName());
        mTotalQuestion.setText("" + testDetails.getTotalQuestion());
        mTotalQuestion1.setText("" + testDetails.getTotalQuestion());
        mTotalQuestion2.setText("" + testDetails.getTotalQuestion());
        mTotalQuestion3.setText("" + testDetails.getTotalQuestion());

        mCorrectAns1.setText("" + testDetails.getTotalRight());
        mCorrectAns.setText("" + testDetails.getTotalRight());
        mInCorrectAns.setText("" + testDetails.getTotalWrong());
        mNotAns.setText("" + (testDetails.getTotalQuestion() - testDetails.getTotalAttempt()));


        mSubjectAccuracy.setProgress(Integer.parseInt(testDetails.getSubjectAccuracy()));
        mSubjectAccuracyLabel.setText("Your Overall Subject Accuracy : " + testDetails.getSubjectAccuracy() + "%");

        mTimeTaken.setText((testDetails.getTakenTime() == 0) ? "00:00:00" : getDurationBreakdown(testDetails.getTakenTime()));
        mAvgTime.setText((testDetails.getAvgTime() == 0) ? "00:00:00" : getDurationBreakdown(testDetails.getAvgTime()));

        testLevelBeans.clear();
        testLevelBeans.addAll(testReport.getLevelQuestions());
        testReportLevelAdapter = new TestReportLevelAdapter(this, testLevelBeans, testReport.getTestDetails().getTotalQuestion());
        mReportLevel.setAdapter(testReportLevelAdapter);

    }
}
