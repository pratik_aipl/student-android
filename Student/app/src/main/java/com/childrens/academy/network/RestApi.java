package com.childrens.academy.network;

import android.support.annotation.NonNull;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("app_version")
    Observable<Response<String>> checkUpdate(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("logout")
    Observable<Response<String>> logOut(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/login/resendotp")
    Observable<Response<String>> reSendOTP(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/confirmotp")
    Observable<Response<String>> confirmOTP(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/subject-list")
    Observable<Response<String>> getSubjectList(@FieldMap Map<String, String> stringMap);

    @GET("level")
    Observable<Response<String>> getLevelList(@QueryMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("querymessage")
    Observable<Response<String>> sendFeedBack(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/searchpaper")
    Observable<Response<String>> getSearchPaperList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/Test_summary_details")
    Observable<Response<String>> getTestSummaryDetails(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/Subject_student_planner")
    Observable<Response<String>> getSubjectStudentPlanner(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/AssessmentPaperlist")
    Observable<Response<String>> getAssessmentPaperList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/LibraryAssessment")
    Observable<Response<String>> getLibraryAssessmentList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/UpdateViewAssessmentPaper")
    Observable<Response<String>> updateViewAssessmentPaper(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/paperlist")
    Observable<Response<String>> getPaperList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/paperquestionlist")
    Observable<Response<String>> getPaperQuestionsList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/submit_test")
    Observable<Response<String>> submitTest(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/test_report")
    Observable<Response<String>> testReport(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("student/test_summary")
    Observable<Response<String>> testSummary(@FieldMap Map<String, String> map);

    @GET("notification_list")
    Observable<Response<String>> getNotificationList(@QueryMap Map<String, String> map);


    @GET("zooki")
    Observable<Response<String>> getZookiList(@QueryMap Map<String, String> map);

    @GET("legal/student")
    Observable<Response<String>> getLegal(@QueryMap Map<String, String> map);

    @GET("student/profile")
    Observable<Response<String>> getProfile(@QueryMap Map<String, String> map);



    //    @Multipart
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "peep/", hasBody = true)
    Observable<Response<String>> peepDelete(@Body RequestBody object);

    String MULTIPART_FORM_DATA = "multipart/form-data";

    static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), s);
    }

}
