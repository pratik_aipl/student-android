package com.childrens.academy;

import android.animation.Animator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.childrens.academy.assessment.Assessment;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.testsection.CCT;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.zookisection.Zooki;
import com.onesignal.OneSignal;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class DashBoard extends ActivityBase {

    private static final String TAG = "DashBoard";
    boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;

    @BindView(R.id.mMenuTest)
    LinearLayout mMenuTest;
    @BindView(R.id.mMenuSearch)
    LinearLayout mMenuSearch;
    @BindView(R.id.mMenuReport)
    LinearLayout mMenuReport;
    @BindView(R.id.mMenuZooki)
    LinearLayout mMenuZooki;
    @BindView(R.id.mAssessment)
    LinearLayout mAssessment;
    @BindView(R.id.mClose)
    ImageView mClose;
    @BindView(R.id.mDashboard)
    TextView mDashboard;
    @BindView(R.id.mMyProfile)
    TextView mMyProfile;
    @BindView(R.id.mQuery)
    TextView mQuery;
    @BindView(R.id.mReferFriend)
    TextView mReferFriend;
    @BindView(R.id.mRateUs)
    TextView mRateUs;
    @BindView(R.id.mLegal)
    TextView mLegal;
    //    @BindView(R.id.mSubScribe)
//    TextView mSubScribe;
//    @BindView(R.id.mNotification)
//    TextView mNotification;
    @BindView(R.id.mLogout)
    TextView mLogout;
    @BindView(R.id.layoutButtons)
    RelativeLayout layoutButtons;
    @BindView(R.id.layoutMain)
    RelativeLayout mLayoutMain;
    @BindView(R.id.layoutContent)
    LinearLayout layoutContent;
    @BindView(R.id.mBackView)
    View mBackView;

    private boolean isOpen = false;
    Subscription subscriptionLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        mHeadNot.setVisibility(View.VISIBLE);
        mPageTitle.setText("Dashboard");
        mBackBtn.setImageResource(R.mipmap.menu);

        OneSignal.idsAvailable((userId, registrationId) -> {
            this.registrationId = userId;
            if (BuildConfig.DEBUG)
                Log.d(TAG, "onCreate: " + userId);
        });

    }

    @OnClick({R.id.mBackBtn, R.id.mBackView, R.id.mClose, R.id.mDashboard, R.id.mMyProfile,
            R.id.mQuery, R.id.mReferFriend, R.id.mRateUs, R.id.mLegal, R.id.mSubScribe,
            R.id.mNotification, R.id.mLogout, R.id.mMenuTest, R.id.mMenuSearch,
            R.id.mMenuReport, R.id.mMenuZooki,R.id.mAssessment, R.id.mHeadNot})
    public void onViewClicked(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.mClose:
            case R.id.mBackBtn:
            case R.id.mBackView:
            case R.id.mDashboard:
                viewMenu();
                break;
            case R.id.mMyProfile:
                viewMenu();
                i = new Intent(this, Profile.class);
                break;
            case R.id.mQuery:
                viewMenu();
                i = new Intent(this, Query.class);
                break;
            case R.id.mReferFriend:

                break;
            case R.id.mRateUs:
                viewMenu();
                launchMarket();
                break;
            case R.id.mLegal:
                viewMenu();
                i = new Intent(this, Legal.class);
                break;
//            case R.id.mSubScribe:
//                break;
//            case R.id.mNotification:
//                break;
            case R.id.mHeadNot:
                i = new Intent(this, Notifications.class);
                break;
            case R.id.mLogout:
                viewMenu();
                Utils.showTwoButtonDialog(this, "Logout", "Are you sure you want logout?", "Logout", "Cancel", new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        callLogOut();
                    }

                    @Override
                    public void onNegativButtonClicked() {
                    }
                });
                break;
            case R.id.mMenuTest:
                if (Utils.isNetworkAvailable(this, true))
                    i = new Intent(this, CCT.class);
                break;
            case R.id.mMenuSearch:
                if (Utils.isNetworkAvailable(this, true))
                    i = new Intent(this, SearchTestPaper.class);
                break;
            case R.id.mMenuReport:
                i = new Intent(this, Report.class);
                break;
            case R.id.mMenuZooki:
                if (Utils.isNetworkAvailable(this, true))
                    i = new Intent(this, Zooki.class);
                break;
            case R.id.mAssessment:
                if (Utils.isNetworkAvailable(this, true))
                    i = new Intent(this, Assessment.class);
                break;
        }
        if (i != null)
            startActivity(i);
    }


    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }


    private void viewMenu() {

        if (!isOpen) {

            int x = layoutContent.getLeft();
            int y = layoutContent.getTop();

            int startRadius = 0;
            int endRadius = (int) Math.hypot(mLayoutMain.getWidth(), mLayoutMain.getHeight());

//            fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), android.R.color.white, null)));
//            fab.setImageResource(R.drawable.ic_close_grey);

            Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
            layoutButtons.setVisibility(View.VISIBLE);
            anim.start();

            isOpen = true;

        } else {

            int x = layoutButtons.getLeft();
            int y = layoutButtons.getTop();

            int startRadius = Math.max(layoutContent.getWidth(), layoutContent.getHeight());
            int endRadius = 0;

//            fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null)));
//            fab.setImageResource(R.drawable.ic_plus_white);

            Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutButtons.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            anim.start();

            isOpen = false;
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Touch again to exit.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        if (subscriptionLogout != null && !subscriptionLogout.isUnsubscribed()) {
            subscriptionLogout.unsubscribe();
            subscriptionLogout = null;
        }
        super.onDestroy();
    }

    private void callLogOut() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        map.put(Constant.PlayerID, Utils.getDeviceId(this));
        showProgress(true);
        subscriptionLogout = NetworkRequest.performAsyncRequest(restApi.logOut(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                prefs.save(Constant.UserData, "");
                prefs.save(Constant.isLogin, false);
                startActivity(new Intent(DashBoard.this, MobileScreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            showConnectionSnackBarUserLogin();
        });
    }
}
