package com.childrens.academy.assessment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.childrens.academy.bean.LevelQuestion;
import com.childrens.academy.bean.QuestionsBean;

import java.util.List;

public class QuestionPagerAdaptor extends FragmentPagerAdapter {
    List<QuestionsBean> questionsBeans;
    long startTime;

    public QuestionPagerAdaptor(FragmentManager fm, List<QuestionsBean> questionsBeans, long startTime) {
        super(fm);
        this.questionsBeans = questionsBeans;
        this.startTime = startTime;
    }

    @Override
    public Fragment getItem(int i) {
          return QuestionPageFragment.newInstance(i, questionsBeans.get(i),startTime);
    }

    @Override
    public int getCount() {
        return questionsBeans.size();
    }
}
