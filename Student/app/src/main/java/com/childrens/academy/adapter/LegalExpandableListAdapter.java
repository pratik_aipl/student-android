package com.childrens.academy.adapter;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.childrens.academy.R;

public class LegalExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;

    public LegalExpandableListAdapter(Context context, List<String> expandableListTitle,
                                      HashMap<String, List<String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
//        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
//        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.mViewContainer);

        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
//        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
//        }
        LinearLayout mViewContainer = (LinearLayout) convertView.findViewById(R.id.mViewContainer);
        View mViewGroup = (View) convertView.findViewById(R.id.mViewGroup);
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.groupTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        if (listPosition == 0) {
            mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
            mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
        } else if (listPosition == expandableListTitle.size() - 1) {
            mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
            mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_left_cornor_round_gray));
        } else {
            mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
            mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.no_cornor_round_gray));
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}