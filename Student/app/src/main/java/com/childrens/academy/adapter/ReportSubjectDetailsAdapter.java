package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.childrens.academy.R;
import com.childrens.academy.SubjectReport;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.bean.SubjectSummaryDetails;
import com.childrens.academy.listner.ReportDetailsSubjectOnClick;
import com.childrens.academy.listner.TestDetailsSubjectOnClick;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class ReportSubjectDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<PaperBean> paperBeanList;
    ReportDetailsSubjectOnClick testOnClick;


    public ReportSubjectDetailsAdapter(SubjectReport context, List<PaperBean> paperBeanList) {
        this.context = context;
        this.paperBeanList = paperBeanList;
        this.testOnClick = context;
    }
//    public TestDetailsSubjectAdapter(SubjectReport context, List<SearchBean> paperBeanList) {
//        this.context = context;
//        this.paperBeanList = paperBeanList;
//        this.testOnClick = context;
//    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_test_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        PaperBean paperBean = paperBeanList.get(position);
       /* if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else */if (position == paperBeanList.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));



        holder.mViewContainer.setOnClickListener(v -> {
            if (!paperBean.getSubmitDate().equalsIgnoreCase("00-00-0000")){
                testOnClick.callbackPaper(position, paperBeanList.get(position));
            }else {
                Toast.makeText(context, "Sorry!! Assignment Missed.", Toast.LENGTH_SHORT).show();
            }
        });

        holder.mAcView.setVisibility(View.VISIBLE);
        holder.mSubjectName.setText(""+paperBean.getSubjectName());
        holder.mRightAns.setText(""+paperBean.getTotalRight());
        holder.mTotalAns.setText(""+paperBean.getTotalQuestion());
        if (!paperBean.getSubmitDate().equalsIgnoreCase("00-00-0000")){
        holder.mEndDate.setText(""+ Utils.changeDateToDDMMMYYYY(paperBean.getSubmitDate()));
        }else {
            holder.mEndDate.setText("Missed");
            holder.mEndDate.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
        }
        holder.mAccuracyLabel.setText("Accuracy "+paperBean.getAccuracy()+"%");
        holder.mAccuracy.setProgress(paperBean.getAccuracy());

//        holder.mTotalAns.setBackground(ContextCompat.getDrawable(context,R.drawable.half_circle_bottom_green));
//        holder.mTotalAns.setTextColor(ContextCompat.getColor(context,R.color.green));
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeanList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mRightAns)
        TextView mRightAns;
        @BindView(R.id.mTotalAns)
        TextView mTotalAns;
        @BindView(R.id.mSubjectName)
        TextView mSubjectName;
        @BindView(R.id.mEndDate)
        TextView mEndDate;
        @BindView(R.id.mAccuracy)
        ProgressBar mAccuracy;
        @BindView(R.id.mAccuracyLabel)
        TextView mAccuracyLabel;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;
        @BindView(R.id.mAcView)
        LinearLayout mAcView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
