package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.Report;
import com.childrens.academy.SubjectReport;
import com.childrens.academy.bean.SearchBean;
import com.childrens.academy.listner.SearchOnClick;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class SearchTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<SearchBean> paperBeanList;
    SearchOnClick testOnClick;

    public SearchTestAdapter(SearchTestPaper context, List<SearchBean> paperBeanList) {
        this.context = context;
        this.paperBeanList = paperBeanList;
        this.testOnClick = context;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_test_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        SearchBean paperBean = paperBeanList.get(position);
        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == paperBeanList.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mViewContainer.setOnClickListener(v -> testOnClick.callbackPaper(position, paperBeanList.get(position)));

        holder.mSubjectName.setText(""+paperBean.getSubjectName());
        holder.mRightAns.setText(""+paperBean.getTotalRight());
        holder.mTotalAns.setText(""+paperBean.getTotalQuestion());
        holder.mEndDate.setText(""+ Utils.changeDateToDDMMMYYYY(paperBean.getSubmitDate()));
        holder.mAccuracyLabel.setText("Accuracy "+paperBean.getAccuracy()+"%");
        holder.mAccuracy.setProgress(paperBean.getAccuracy());
//        holder.mTotalAns.setText(""+);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeanList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mRightAns)
        TextView mRightAns;
        @BindView(R.id.mTotalAns)
        TextView mTotalAns;
        @BindView(R.id.mSubjectName)
        TextView mSubjectName;
        @BindView(R.id.mEndDate)
        TextView mEndDate;
        @BindView(R.id.mAccuracy)
        ProgressBar mAccuracy;
        @BindView(R.id.mAccuracyLabel)
        TextView mAccuracyLabel;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
