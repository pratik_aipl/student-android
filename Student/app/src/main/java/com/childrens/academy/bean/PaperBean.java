package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class PaperBean implements Serializable {

    @JsonField(name = "MCQPlannerID")
    String MCQPlannerID;

    @JsonField(name = "PlannerName")
    String PlannerName;


    @JsonField(name = "StudentMCQTestID")
    String StudentMCQTestID;

    @JsonField(name = "with_question")
    String with_question;

    @JsonField(name = "TotalQuestion")
    int TotalQuestion;

    public List<QuestionsBean> getPaper_question() {
        return Paper_question;
    }

    public void setPaper_question(List<QuestionsBean> paper_question) {
        Paper_question = paper_question;
    }

    @JsonField(name = "TotalRight")
    int TotalRight;

    @JsonField(name = "SubjectName")
    String SubjectName;

    @JsonField(name = "StandardName")
    String StandardName;

    @JsonField(name = "MediumName")
    String MediumName;

    @JsonField(name = "BoardName")
    String BoardName;

    @JsonField(name = "BranchName")
    String BranchName;

    @JsonField(name = "BranchID")
    int BranchID;

    @JsonField(name = "StartDate")
    String StartDate;

    @JsonField(name = "EndDate")
    String EndDate;
    @JsonField(name = "SubmitDate")
    String SubmitDate;

    @JsonField(name = "PublishDate")
    String PublishDate;

    @JsonField(name = "Time")
    String Time;

    @JsonField(name = "Type")
    String Type;
    @JsonField(name = "VideoID")
    String VideoID;
    @JsonField(name = "StartTime")
    long StartTime;
    @JsonField(name = "EndTime")
    long EndTime;
    @JsonField(name = "VideoName")
    String VideoName;
    @JsonField(name = "VideoImage")
    String VideoImage;
    @JsonField(name = "VideoURL")
    String VideoURL;

    @JsonField(name = "chapter")
    List<ChapterBean> chapterList;

    @JsonField(name = "Paper_question")
    List<QuestionsBean> Paper_question;

    @JsonField(name = "Accuracy")
    int Accuracy;

    public String getPlannerName() {
        return PlannerName;
    }

    public String getWith_question() {
        return with_question;
    }

    public void setWith_question(String with_question) {
        this.with_question = with_question;
    }

    public void setPlannerName(String plannerName) {
        PlannerName = plannerName;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }

    public String getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(String studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getVideoID() {
        return VideoID;
    }

    public void setVideoID(String videoID) {
        VideoID = videoID;
    }

    public long getStartTime() {
        return StartTime;
    }

    public void setStartTime(long startTime) {
        StartTime = startTime;
    }

    public long getEndTime() {
        return EndTime;
    }

    public void setEndTime(long endTime) {
        EndTime = endTime;
    }

    public String getVideoName() {
        return VideoName;
    }

    public void setVideoName(String videoName) {
        VideoName = videoName;
    }

    public String getVideoImage() {
        return VideoImage;
    }

    public void setVideoImage(String videoImage) {
        VideoImage = videoImage;
    }

    public String getVideoURL() {
        return VideoURL;
    }

    public void setVideoURL(String videoURL) {
        VideoURL = videoURL;
    }

    public List<ChapterBean> getChapterList() {
        return chapterList;
    }

    public void setChapterList(List<ChapterBean> chapterList) {
        this.chapterList = chapterList;
    }


    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public int getBranchID() {
        return BranchID;
    }


    public void setBranchID(int branchID) {
        BranchID = branchID;
    }

    public String getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(String MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
