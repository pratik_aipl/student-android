package com.childrens.academy.assessment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.childrens.academy.BuildConfig;
import com.childrens.academy.FullScreenActivity;
import com.childrens.academy.R;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.AnswerDetailBean;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.listner.AssessmentPageReload;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.listner.OptionsOnClick;
import com.childrens.academy.listner.PageReloadEvent;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.testsection.TodayTestReport;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.QuestionsWebView;
import com.childrens.academy.view.xvideoplayer.MxMediaManager;
import com.childrens.academy.view.xvideoplayer.MxUserAction;
import com.childrens.academy.view.xvideoplayer.MxVideoPlayer;
import com.childrens.academy.view.xvideoplayer.MxVideoPlayerWidget;
import com.childrens.academy.view.xvideoplayer.PlayerPause;
import com.childrens.academy.view.xvideoplayer.QuestionAction;
import com.childrens.academy.view.xvideoplayer.UpdatePlayerData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscription;

import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_AUTO_COMPLETE;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_ERROR;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_NORMAL;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PAUSE;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING_BUFFERING_START;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.SCREEN_WINDOW_FULLSCREEN;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.backPress;

public class TestVideoAssessment extends ActivityBase implements NetworkStateReceiver.NetworkStateReceiverListener, OptionsOnClick {

    private static final String TAG = "TodayTest";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;

    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;

    @BindView(R.id.mVideoView1)
    MxVideoPlayerWidget videoPlayerWidget;
    @BindView(R.id.mVolume)
    ImageButton mVolume;
    @BindView(R.id.forward)
    ImageButton forward;
    @BindView(R.id.play_pause_video)
    ImageButton playPauseVideo;
    @BindView(R.id.rewind)
    ImageButton rewind;
    @BindView(R.id.mZoomBtn)
    ImageButton mZoomBtn;
    @BindView(R.id.play_time)
    TextView playTime;
    @BindView(R.id.mDurations)
    TextView mDurations;
    @BindView(R.id.mProgressVideo)
    SeekBar mProgressVideo;
    @BindView(R.id.mPointerView)
    RelativeLayout mPointerView;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;


    @BindView(R.id.mQuestionText)
    QuestionsWebView mQuestionText;
    @BindView(R.id.mOptionList)
    RecyclerView mOptionList;
    @BindView(R.id.mRewatch)
    Button mRewatch;
    @BindView(R.id.mContinue)
    Button mContinue;
    @BindView(R.id.mViewContainer)
    LinearLayout mViewContainer;
    @BindView(R.id.mShowScreen)
    RelativeLayout mShowScreen;
    Unbinder unbinder;

    OptionAdapter optionAdapter;

    int selPosion;
    long lastPoint = 0, tempLastPoint = 0;
    boolean isShowScreen = false, isShowLast = false;
    List<OptionBean> optionBeans = new ArrayList<>();
    @BindView(R.id.mQuestionView)
    RelativeLayout mQuestionView;
    @BindView(R.id.mButtonView)
    LinearLayout mButtonView;

    boolean isAutoEndTest = false,from = false;
    PaperBean paperBean;
    Subscription subscriptionQuestionsList, subscriptionSubmitTest;
    List<QuestionsBean> questionsBeans = new ArrayList<>();
    boolean isAlertShow = false;
    public NetworkStateReceiver networkStateReceiver;

    int mCurrentScreen = CURRENT_STATE_NORMAL, mCurrentState = 0;
    Subscription subscriptionPaperList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        unSubScribeEvent(true);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        from = getIntent().getBooleanExtra(Constant.from, false);

        mEndTest.setVisibility(View.GONE);
        mRewatch.setVisibility(View.GONE);

        mPageTitle.setText(paperBean.getPlannerName());
        mDurations.setText(Utils.stringForTime(paperBean.getEndTime() - paperBean.getStartTime()));
        mProgressVideo.setMax((int) (paperBean.getEndTime() - paperBean.getStartTime()));
        playTime.setText("00:00");
        lastPoint = paperBean.getStartTime();
        videoPlayerWidget.startPlay(paperBean.getVideoURL().replace("https", "http"), paperBean.getStartTime(), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, "");
        Utils.loadImageWithPicasso(this, paperBean.getVideoImage(), videoPlayerWidget.mThumbImageView, mProgress);
//        if (Utils.isNetworkAvailable(this, true))
//            getQuestionsList(true);

        questionsBeans.clear();
        questionsBeans.addAll(paperBean.getPaper_question());

        for (int i = 0; i < questionsBeans.size(); i++) {
            SeekBar seekBar = new SeekBar(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 0);
            seekBar.setLayoutParams(layoutParams);
            seekBar.setThumb(ContextCompat.getDrawable(this, R.mipmap.pointer));
            seekBar.setProgressDrawable(ContextCompat.getDrawable(this, R.drawable.seek_trans_dra));
            seekBar.setMax((int) (paperBean.getEndTime() - paperBean.getStartTime()));
            seekBar.setProgress((int) (questionsBeans.get(i).getQuestionMarkers() - paperBean.getStartTime()));
            mPointerView.addView(seekBar);
        }


        if (mCurrentScreen == SCREEN_WINDOW_FULLSCREEN) {
            mZoomBtn.setImageResource(R.mipmap.ic_exitfullscreen);
        } else {
            mZoomBtn.setImageResource(R.mipmap.ic_fullscreen);
        }

    }

    private void updateViewAssessmentPaper(boolean isShow, PaperBean paperBean, int pos) {
        Log.d(TAG, "updateViewAssessmentPaper: ");
        if (from) {

            Map<String, String> map = new HashMap<>();
            map.put(Constant.isViewed, "1");
            map.put(Constant.PaperID, paperBean.getMCQPlannerID());
            showProgress(isShow);
            subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.updateViewAssessmentPaper(map), (data) -> {
                showProgress(false);
                if (data.code() == 200) {
                    MxMediaManager.getInstance().getPlayer().release();
                    EventBus.getDefault().post(new AssessmentPageReload(true));
                    startActivity(new Intent(this, Library.class));
                    finish();
                } else {
                    Utils.serviceStatusFalseProcess(this, data);
                }

            }, (e) -> {
                showProgress(false);
                e.printStackTrace();
            });
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerPauseEvent(PlayerPause event) {
        mCurrentScreen = event.getmCurrentScreen();
        mCurrentState = event.getState();
        Log.d(TAG, "onPlayerPauseEvent: " + event.getState());
        if (mCurrentState == CURRENT_STATE_NORMAL || mCurrentState == CURRENT_STATE_ERROR) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_PLAYING) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
        } else if (mCurrentState == CURRENT_STATE_PLAYING_BUFFERING_START) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
        } else if (mCurrentState == CURRENT_STATE_PAUSE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_AUTO_COMPLETE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
            if (Utils.isNetworkAvailable(TestVideoAssessment.this, true)) {
                updateViewAssessmentPaper(true, paperBean, 0);
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestVideoAssesment.this);
//                alertDialogBuilder.setTitle("");
//                alertDialogBuilder.setCancelable(false);
//                alertDialogBuilder.setMessage("Video assignment is completed.\nPlease click OK to see your performance.");
//                alertDialogBuilder.setPositiveButton("OK", (dialog, which) -> submitTest(true)).show();
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdatePlayerDataEvent(UpdatePlayerData event) {
        playTime.setText(Utils.stringForTime((event.getCurrentTimeP() - paperBean.getStartTime())));
        mProgressVideo.setProgress((int) (event.getCurrentTimeP() - paperBean.getStartTime()));
        if (event.getCurrentTimeP() >= paperBean.getEndTime()) {
            videoPlayerWidget.obtainCache();
            videoPlayerWidget.onActionEvent(MxUserAction.ON_CLICK_PAUSE);
            updateViewAssessmentPaper(true, paperBean, 0);
        } else {
            pauseAction();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onQuestionActionEvent(QuestionAction event) {
        if (event.isContinue()) {
            questionsBeans.add(event.getSelPosion(), event.getQuestionsBean());
            playPauseVideo.setClickable(true);
            playPauseVideo.setEnabled(true);
            playPauseVideo.performClick();
        } else {
            videoPlayerWidget.resetProgressAndTime();
        }

    }

    private void unSubScribeEvent(boolean isSubScribe) {
        if (isSubScribe && !EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        } else if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    public void pauseAction() {

        for (int i = 0; i < questionsBeans.size(); i++) {
            QuestionsBean questionsBean = questionsBeans.get(i);
            Log.d(TAG, "pauseAction: " + questionsBeans.size());
            if (TimeUnit.MILLISECONDS.toSeconds(questionsBean.getQuestionMarkers() - paperBean.getStartTime()) == TimeUnit.MILLISECONDS.toSeconds(mProgressVideo.getProgress()) && !questionsBean.isAnswerd()) {
                videoPlayerWidget.obtainCache();
                videoPlayerWidget.onActionEvent(MxUserAction.ON_CLICK_PAUSE);
                MxMediaManager.getInstance().getPlayer().pause();
                videoPlayerWidget.setUiPlayState(CURRENT_STATE_PAUSE);
                videoPlayerWidget.refreshCache();
                onDisableView(i, questionsBean, ((questionsBeans.size() - 1) == i));
                break;
            }
        }

    }

    //  @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDisableView(int positions, QuestionsBean questionsBean, boolean isLast) {
        playPauseVideo.setClickable(false);
        playPauseVideo.setEnabled(false);
        selPosion = positions;
        if (!isShowScreen && !isShowLast) {
            isShowScreen = true;
            mShowScreen.setVisibility(View.VISIBLE);

            if (isLast)
                isShowLast = isLast;

            mQuestionText.loadHtmlFromLocal(questionsBean.getQuestion());
            mQuestionText.setInitialScale(30);
            tempLastPoint = MxMediaManager.getInstance().getPlayer().getCurrentPosition();

            mQuestionText.setBackgroundColor(Color.TRANSPARENT);
            mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            mQuestionText.setScrollbarFadingEnabled(false);
            mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
            mQuestionText.addJavascriptInterface(new WebInterface(this), "Android");

            optionBeans.clear();
            optionBeans.addAll(questionsBean.getOptions());
            mOptionList.setLayoutManager(new LinearLayoutManager(this));
            optionAdapter = new OptionAdapter(TestVideoAssessment.this, optionBeans, false, 0, 0, false, 0);
            mOptionList.setAdapter(optionAdapter);
        }
    }

    public class WebInterface {
        Context mContext;

        public WebInterface(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String toast) {
            startActivity(new Intent(mContext, FullScreenActivity.class).putExtra(Constant.imagePath, toast));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + isAlertShow);
    }

    private void callRewatch() {
        Log.d(TAG, "callRewatch: " + lastPoint);
        playPauseVideo.setClickable(true);
        playPauseVideo.setEnabled(true);
        isShowScreen = false;
        mShowScreen.setVisibility(View.GONE);
        MxMediaManager.getInstance().getPlayer().seekTo(lastPoint);
        MxMediaManager.getInstance().getPlayer().start();
        videoPlayerWidget.setUiPlayState(CURRENT_STATE_PLAYING);
        playPauseVideo.setImageResource(R.mipmap.ic_pause);


    }

    private void callContinue() {
        boolean isCheck = false;
        for (int j = 0; j < optionBeans.size(); j++) {
            OptionBean optionBean = optionBeans.get(j);
            if (optionBean.isSelected()) {
                isCheck = true;
                break;
            }
        }
        if (isCheck) {
            questionsBeans.get(selPosion).setAnswerd(true);
            playPauseVideo.setClickable(true);
            playPauseVideo.setEnabled(true);
            videoPlayerWidget.obtainCache();
            lastPoint = tempLastPoint;
            tempLastPoint = 0;
            videoPlayerWidget.onActionEvent(MxUserAction.ON_CLICK_RESUME);
            MxMediaManager.getInstance().getPlayer().start();
            videoPlayerWidget.setUiPlayState(CURRENT_STATE_PLAYING);
//            videoPlayerWidget.refreshCache();
            isShowScreen = false;
            mShowScreen.setVisibility(View.GONE);
        } else {
            Toast.makeText(this, "Please select answer.", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.mBackBtn, R.id.mEndTest, R.id.play_pause_video, R.id.mZoomBtn, R.id.mRewatch, R.id.mContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.play_pause_video:

                if (mCurrentState == 0) {
                    videoPlayerWidget.mThumbImageView.performClick();
                    videoPlayerWidget.mThumbImageView.setVisibility(View.GONE);
                } else {
                    videoPlayerWidget.mPlayControllerButton.performClick();
                }
                break;
            case R.id.mRewatch:
                callRewatch();
                break;
            case R.id.mContinue:
                callContinue();
                break;
            case R.id.mZoomBtn:
                if (mCurrentScreen == SCREEN_WINDOW_FULLSCREEN) {
                    videoPlayerWidget.mFullscreenButton.performClick();
                } else {
                    backPress();
                }
                break;
            case R.id.mEndTest:
                if (Utils.isNetworkAvailable(TestVideoAssessment.this, true)) {
                    isAlertShow = true;

                    Utils.showTwoButtonDialog(this, "End Test", "Do you really want to submit the test?", "Submit", "Cancel", new DialogButtonListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            submitTest(true);
                        }

                        @Override
                        public void onNegativButtonClicked() {
                            isAlertShow = false;
                        }

                    });
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        MxMediaManager.getInstance().releaseMediaPlayer();
        finish();
    }

    private void submitTest(boolean isShow) {

        List<AnswerDetailBean> answerDetailBeans = new ArrayList<>();
        for (int i = 0; i < questionsBeans.size(); i++) {
            OptionBean selectOption = new OptionBean();
            for (int j = 0; j < questionsBeans.get(i).getOptions().size(); j++) {
                OptionBean optionBean = questionsBeans.get(i).getOptions().get(j);
                if (optionBean.isSelected()) {
                    selectOption.setMCQOptionID(optionBean.getMCQOptionID());
                    selectOption.setAttempt(optionBean.isAttempt());
                    break;
                }
                selectOption.setMCQOptionID(optionBean.getMCQOptionID());
            }
            answerDetailBeans.add(new AnswerDetailBean(questionsBeans.get(i).getMCQPlannerDetailsID(), selectOption.isAttempt() ? selectOption.getMCQOptionID() : 0, selectOption.isAttempt() ? "1" : "0"));
        }

        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, paperBean.getMCQPlannerID());
        map.put(Constant.detailArray, gson.toJson(answerDetailBeans));
        // TODO: 25/3/2019 taken time for video
        map.put(Constant.TakenTime, String.valueOf(mProgressVideo.getMax()));
        if (BuildConfig.DEBUG)
            Log.d(TAG, "submitTest: sample data " + gson.toJson(answerDetailBeans));
        showProgress(isShow);
        subscriptionSubmitTest = NetworkRequest.performAsyncRequest(restApi.submitTest(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                    if (jsonObject.has(Constant.StudentMCQTestID)) {
                        startActivity(new Intent(this, TodayTestReport.class)
                                .putExtra(Constant.isTest, true)
                                .putExtra(Constant.StudentMCQTestID, jsonObject.getString(Constant.StudentMCQTestID))
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    protected void onDestroy() {
        if (subscriptionQuestionsList != null && !subscriptionQuestionsList.isUnsubscribed()) {
            subscriptionQuestionsList.unsubscribe();
            subscriptionQuestionsList = null;
        }
        if (subscriptionSubmitTest != null && !subscriptionSubmitTest.isUnsubscribed()) {
            subscriptionSubmitTest.unsubscribe();
            subscriptionSubmitTest = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        super.onDestroy();
        unSubScribeEvent(true);
    }


    @Override
    public void networkAvailable() {
        if (isAutoEndTest)
            submitTest(true);
    }

    @Override
    public void networkUnavailable() {
    }

    @Override
    public void callbackOption(int pos, OptionBean paperBean) {

        for (int i = 0; i < optionBeans.size(); i++) {
            optionBeans.get(i).setClickPos(i);
            optionBeans.get(pos).setAttempt(true);
            optionBeans.get(pos).setSelected(true);
        }
        mOptionList.setLayoutManager(new LinearLayoutManager(this));
        optionAdapter = new OptionAdapter(TestVideoAssessment.this, optionBeans, false, 0, 0, false, 0);
        mOptionList.setAdapter(optionAdapter);
    }

}
