package com.childrens.academy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.childrens.academy.bean.NotificationBean;
import com.childrens.academy.listner.NotificationOnClick;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<NotificationBean> notificationBeans;
    NotificationOnClick notificationOnClick;

    public NotificationAdapter(Notifications notifications, List<NotificationBean> notificationBeans) {
        this.context = notifications;
        this.notificationBeans = notificationBeans;
        this.notificationOnClick = notifications;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_notification, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        NotificationBean bean = notificationBeans.get(position);

        holder.mTitle.setText(bean.getNotificationTitle());
        holder.mMessage.setText(bean.getNotificationMsg());
        holder.tvDate.setText(Utils.printDifference(bean.getCreatedDate()));
        holder.relLeft.setOnClickListener(v -> notificationOnClick.callNotificationClick(position,bean));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return notificationBeans.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mTitle)
        TextView mTitle;
        @BindView(R.id.mMessage)
        TextView mMessage;
        @BindView(R.id.rel_left)
        RelativeLayout relLeft;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
