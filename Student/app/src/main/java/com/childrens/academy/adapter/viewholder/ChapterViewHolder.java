package com.childrens.academy.adapter.viewholder;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;


import com.childrens.academy.R;
import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.expan.Genre;
import com.childrens.academy.view.expandablerecyclerview.models.ExpandableGroup;
import com.childrens.academy.view.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class ChapterViewHolder extends GroupViewHolder {

  private TextView mChapterName;
  private ImageView arrow;
  public ChapterViewHolder(View itemView) {
    super(itemView);
    mChapterName = (TextView) itemView.findViewById(R.id.mChapterName);
    arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);
  }

  public void setGenreTitle(ExpandableGroup genre) {
    if (genre instanceof Genre) {
      mChapterName.setText("* "+genre.getTitle());
    }
//    if (genre instanceof MultiCheckGenre) {
//      genreName.setText(genre.getTitle());
//      icon.setBackgroundResource(((MultiCheckGenre) genre).getIconResId());
//    }
//    if (genre instanceof SingleCheckGenre) {
//      genreName.setText(genre.getTitle());
//      icon.setBackgroundResource(((SingleCheckGenre) genre).getIconResId());
//    }
  }

  @Override
  public void expand() {
    animateExpand();
  }

  @Override
  public void collapse() {
    animateCollapse();
  }

  private void animateExpand() {
    RotateAnimation rotate =
        new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }

  private void animateCollapse() {
    RotateAnimation rotate =
        new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }
}
