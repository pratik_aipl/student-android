package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class NotificationBean implements Serializable {
/*
      "NotificationID": "2",
      "UserID": "1430",
      "NotificationTitle": "A",
      "NotificationMsg": "A",
      "NotificationImage": "A",
      "Type": "custom_notification",
      "CreatedBy": "1",
      "CreatedDate": "2019-03-25 00:00:00"
 */

    @JsonField
    int NotificationID;
    @JsonField
    int UserID;

    @JsonField
    String NotificationTitle;
    @JsonField
    String NotificationMsg;
    @JsonField
    String NotificationImage;
    @JsonField
    String Type;
    @JsonField
    String CreatedBy;
    @JsonField
    String CreatedDate;


    public int getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(int notificationID) {
        NotificationID = notificationID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getNotificationTitle() {
        return NotificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        NotificationTitle = notificationTitle;
    }

    public String getNotificationMsg() {
        return NotificationMsg;
    }

    public void setNotificationMsg(String notificationMsg) {
        NotificationMsg = notificationMsg;
    }

    public String getNotificationImage() {
        return NotificationImage;
    }

    public void setNotificationImage(String notificationImage) {
        NotificationImage = notificationImage;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
