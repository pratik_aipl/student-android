package com.childrens.academy;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.adapter.CCTAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.NotificationBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.InternetConnetionLostEvent;
import com.childrens.academy.listner.NotificationOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Notifications extends ActivityBase implements NetworkStateReceiver.NetworkStateReceiverListener, NotificationOnClick {


    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;
    Snackbar snackbar;
    List<NotificationBean> notificationBeanList = new ArrayList<>();
    Subscription subscriptionNotificationList;
    public NetworkStateReceiver networkStateReceiver;
    android.support.v7.app.AlertDialog alertDialog;
    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noyifications);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mPageTitle.setText("Notifications");
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        notificationAdapter = new NotificationAdapter(this, notificationBeanList);
        mRecyclerView.setAdapter(notificationAdapter);
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Notifications Loading...");
        if (notificationBeanList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }

    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }

    private void getNotificationsList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionNotificationList = NetworkRequest.performAsyncRequest(restApi.getNotificationList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    notificationBeanList.clear();
                    notificationBeanList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), NotificationBean.class));
                    notificationAdapter.notifyDataSetChanged();
                    mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
                    tvEmpty.setText(jsonResponse.getString(Constant.message)/*"Notifications not available"*/);
                    if (notificationBeanList.size() > 0) {
                        if (snackbar != null && snackbar.isShown())
                            snackbar.dismiss();
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mEmptyView.setVisibility(View.GONE);
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInternetConnetionLostEvent(InternetConnetionLostEvent event) {
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Network Connections Error!!");
        if (notificationBeanList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        callSnackBar();
    }

    @Override
    public void networkAvailable() {
        getNotificationsList(true);

    }

    private void callSnackBar() {
        snackbar = Snackbar
                .make(mBackBtn, "Please check internet connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> {
                    if (Utils.isNetworkAvailable(Notifications.this, false))
                        getNotificationsList(true);
                    else
                        callSnackBar();
                });
        snackbar.show();
    }


    @Override
    public void onDestroy() {
        if (subscriptionNotificationList != null && !subscriptionNotificationList.isUnsubscribed()) {
            subscriptionNotificationList.unsubscribe();
            subscriptionNotificationList = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    @Override
    public void networkUnavailable() {
        mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
        tvEmpty.setText("Network Connections Error!!");
        if (notificationBeanList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        callSnackBar();
    }

    @Override
    public void callNotificationClick(int pos, NotificationBean notificationBean) {

    }
}
