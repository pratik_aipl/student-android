package com.childrens.academy.view.expandablerecyclerview.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * ViewHolder for {@link com.childrens.academy.view.expandablerecyclerview.models.ExpandableGroup#items}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

  public ChildViewHolder(View itemView) {
    super(itemView);
  }
}
