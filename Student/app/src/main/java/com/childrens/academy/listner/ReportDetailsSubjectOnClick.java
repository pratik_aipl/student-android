package com.childrens.academy.listner;

import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.SubjectSummaryDetails;

public interface ReportDetailsSubjectOnClick {
    public void callbackPaper(int pos, PaperBean searchBean);
}
