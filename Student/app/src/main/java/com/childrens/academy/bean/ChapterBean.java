package com.childrens.academy.bean;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;
import com.childrens.academy.view.expandablerecyclerview.models.ExpandableGroup;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ChapterBean implements Serializable {

    @JsonField(name = "ChapterID")
    int ChapterID;
    @JsonField(name = "ChapterName")
    String ChapterName;
    @JsonField(name = "sub_chapter")
    List<ChapterBean> subChapter;

    @JsonField(name = "Paper_info")
    List<PaperBean> Paper_info;

    public List<PaperBean> getPaper_info() {
        return Paper_info;
    }

    public void setPaper_info(List<PaperBean> paper_info) {
        Paper_info = paper_info;
    }

    public int getChapterID() {
        return ChapterID;
    }

    public void setChapterID(int chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public List<ChapterBean> getSubChapter() {
        return subChapter;
    }

    public void setSubChapter(List<ChapterBean> subChapter) {
        this.subChapter = subChapter;
    }
}
