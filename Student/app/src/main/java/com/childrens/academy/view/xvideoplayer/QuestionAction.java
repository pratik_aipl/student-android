package com.childrens.academy.view.xvideoplayer;

import com.childrens.academy.bean.QuestionsBean;

public class QuestionAction {
    QuestionsBean questionsBean;
    boolean isContinue;
    int selPosion;

    public QuestionAction(QuestionsBean questionsBean,int selPosion,boolean isContinue) {
        this.selPosion = selPosion;
        this.isContinue = isContinue;
        this.questionsBean = questionsBean;
    }

    public QuestionsBean getQuestionsBean() {
        return questionsBean;
    }

    public boolean isContinue() {
        return isContinue;
    }

    public int getSelPosion() {
        return selPosion;
    }
}
