package com.childrens.academy.service;

public interface SmsListener
{
    void messageReceived(String messageText);
}