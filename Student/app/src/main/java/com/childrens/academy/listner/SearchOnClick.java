package com.childrens.academy.listner;


import com.childrens.academy.bean.SearchBean;

public interface SearchOnClick {
    public void callbackPaper(int pos, SearchBean searchBean);
}
