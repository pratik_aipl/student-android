package com.childrens.academy.bean;

public class AnswerDetailBean {

    int MCQPlannerDetailsID;
    int AnswerID;
    String IsAttempt;
    String selectOption;

    public AnswerDetailBean() {
    }

    public AnswerDetailBean(int MCQPlannerDetailsID, int answerID, String isAttempt) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
        AnswerID = answerID;
        IsAttempt = isAttempt;
    }
    public AnswerDetailBean(int MCQPlannerDetailsID, int answerID, String isAttempt, String selectOption) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
        AnswerID = answerID;
        IsAttempt = isAttempt;
        this.selectOption = selectOption;
    }

    public int getMCQPlannerDetailsID() {
        return MCQPlannerDetailsID;
    }

    public int getAnswerID() {
        return AnswerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public String getSelectOption() {
        return selectOption;
    }
}
