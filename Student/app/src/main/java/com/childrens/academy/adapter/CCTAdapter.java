package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.assessment.Assessment;
import com.childrens.academy.assessment.Library;
import com.childrens.academy.assessment.LibraryPaperInfo;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.testsection.CCT;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class CCTAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "CCTAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<PaperBean> paperBeans;
    TestOnClick testOnClick;
    boolean isLib = false;
    boolean isAssessment = false;

    public CCTAdapter(Assessment cct, List<PaperBean> paperBeans) {
        this.isAssessment = true;
        this.context = cct;
        this.paperBeans = paperBeans;
        this.testOnClick = cct;
    }

    public CCTAdapter(LibraryPaperInfo cct, List<PaperBean> paperBeans) {
        this.isLib = true;
        this.context = cct;
        this.paperBeans = paperBeans;
        this.testOnClick = cct;
    }

    public CCTAdapter(CCT cct, List<PaperBean> paperBeans) {
        this.context = cct;
        this.paperBeans = paperBeans;
        this.testOnClick = cct;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_cct_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        PaperBean paperBean = paperBeans.get(position);

        if (isLib) {
            holder.mSubIcon.setVisibility(View.VISIBLE);
            holder.mDateView.setVisibility(View.GONE);
        } else {
            holder.mSubIcon.setVisibility(View.GONE);
            holder.mDateView.setVisibility(View.VISIBLE);
            holder.mDay.setText(context.getString(R.string.date_on, Utils.changeDateToDay(paperBean.getStartDate()), Utils.changeDateToMonth(paperBean.getStartDate())));
        }
        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == paperBeans.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mViewContainer.setOnClickListener(v -> testOnClick.callbackPeep(position, paperBean));
        if (isLib)
            holder.mPaperName.setText(paperBean.getPlannerName());
        else if (isAssessment)
            holder.mPaperName.setText(paperBean.getPlannerName());
        else
            holder.mPaperName.setText(paperBean.getSubjectName());

        if (!TextUtils.isEmpty(paperBean.getPublishDate()) && paperBean.getPublishDate() != "null")
            holder.mExpireDate.setText(context.getString(R.string.submition_on, Utils.changeDate(paperBean.getPublishDate())));
        else
            holder.mExpireDate.setVisibility(View.GONE);


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mDay)
        TextView mDay;
        @BindView(R.id.mMonth)
        TextView mMonth;
        @BindView(R.id.mPaperName)
        TextView mPaperName;
        @BindView(R.id.mExpireDate)
        TextView mExpireDate;
        @BindView(R.id.mNextArrow)
        ImageView mNextArrow;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;
        @BindView(R.id.mSubIcon)
        ImageView mSubIcon;
        @BindView(R.id.mDateView)
        LinearLayout mDateView;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
