package com.childrens.academy.assessment;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;


import com.childrens.academy.R;
import com.childrens.academy.assessment.view.OptionsWebView;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.listner.OptionsOnClick;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pratik on 04/12/18.
 */
public class OptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OptionAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<OptionBean> optionBeans;
    private RadioButton mCheckAddressTemp = null;
    private RadioButton mCheckAddressRight = null;
    private int lastCheckedPos = 0;
    private int rightCheckedPos = 0;
    //    private int answerID = 0;
    boolean isStudent;
    //    int isCorrect;
//    boolean isFromPlanner;
    OptionsOnClick optionsOnClick;
    double performance;


    public OptionAdapter(QuestionPageFragment context, List<OptionBean> optionBeans, boolean isStudent, int answerID, int isCorrect, boolean isFromPlanner, double performance) {
        this.context = context.getActivity();
        this.optionBeans = optionBeans;
        this.isStudent = isStudent;
        this.performance = performance;
        this.optionsOnClick = context;
    }


    public OptionAdapter(TestVideoAssessment context, List<OptionBean> optionBeans, boolean isStudent, int answerID, int isCorrect, boolean isFromPlanner, double performance) {
        this.context = context;
        this.optionBeans = optionBeans;
        this.isStudent = isStudent;
        this.performance = performance;
        this.optionsOnClick = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_question_option_item_assessment, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        OptionBean beanData = optionBeans.get(position);
//        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.mOptionsText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mOptionsText.setScrollbarFadingEnabled(false);
        holder.mOptionsText.setInitialScale(30);
        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        holder.mRadioBtn.setText(Utils.getOptionText(position));
        holder.mViewContainer.bringToFront();

//        Log.d(TAG, "onBindViewHolder: " + beanData.getOptions());
//        Log.d(TAG, "onBindViewHolder: " + beanData.getOptions().replace("<p>","<p color: #5ECE88 >"));
        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == optionBeans.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        if (beanData.isSelected()) {
            Log.d(TAG, "onBindViewHolder: " + beanData.getIsCorrect());

            if (beanData.getClickPos() == position && beanData.getIsCorrect().equalsIgnoreCase("0")) {
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #FF0000;\">"), "#FF0000");
            } else {
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #000000;\">"), "#000000");
            }

            if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                holder.mAnsAction.setVisibility(View.VISIBLE);
                holder.mAnsAction.setImageResource(R.mipmap.rigth);
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.green));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #5ECE88;\">"), "#5ECE88");
            }


        } else {
            holder.mAnsAction.setVisibility(View.GONE);
            if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                mCheckAddressRight = holder.mRadioBtn;
                rightCheckedPos = position;
            }
            if (beanData.isSelected()) {
                mCheckAddressTemp = holder.mRadioBtn;
                lastCheckedPos = position;
            } else {
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #000000;\">"), "#000000");

            }
            if (beanData.getClickPos() != -1 && beanData.getIsCorrect().equalsIgnoreCase("1")) {
                holder.mAnsAction.setVisibility(View.VISIBLE);
                holder.mAnsAction.setImageResource(R.mipmap.rigth);
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.green));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #5ECE88;\">"), "#5ECE88");
            }
        }

        holder.mOptionsText.setOnTouchListener(new View.OnTouchListener() {
            private long startClickTime;

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    startClickTime = System.currentTimeMillis();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (System.currentTimeMillis() - startClickTime < ViewConfiguration.getTapTimeout()) {
                        if (view.getId() == R.id.mOptionsText) {
                            holder.mViewContainer.performClick();
                        }
                    } else {
                        // Touch was a not a simple tap.
                    }

                }

                return true;
            }
        });

        holder.mViewContainer.setOnClickListener(v -> {
//            if (mCheckAddressTemp != null) {
////                optionBeans.get(lastCheckedPos).setSelected(false);
////                mCheckAddressTemp.setChecked(false);
////            }
//////            if (mCheckAddressRight != null) {
//////                optionBeans.get(rightCheckedPos).setSelected(true);
//////                mCheckAddressRight.setChecked(false);
//////            }
////            mCheckAddressTemp = holder.mRadioBtn;
////            lastCheckedPos = position;
////            optionBeans.get(position).setSelected(true);
////            optionBeans.get(position).setAttempt(true);
////            optionBeans.get(position).setClickPos(position);
////
////            notifyDataSetChanged();
            optionsOnClick.callbackOption(position, beanData);
        });
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return optionBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        public LinearLayout mViewContainer;
        @BindView(R.id.mOptionsText)
        public OptionsWebView mOptionsText;
        @BindView(R.id.mRadioBtn)
        public RadioButton mRadioBtn;
        @BindView(R.id.mOptionLabelView)
        public ImageView mOptionLabelView;
        @BindView(R.id.mAnsAction)
        public ImageView mAnsAction;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
