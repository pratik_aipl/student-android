package com.childrens.academy.testsection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.adapter.ChapterAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.ChapterBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.expan.Artist;
import com.childrens.academy.bean.expan.Genre;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestInfo extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mTimer)
    TextView mTimer;
    @BindView(R.id.mChapter)
    TextView mChapter;
    @BindView(R.id.mChapterName)
    RecyclerView mChapterName;

    PaperBean paperBean;
    @BindView(R.id.mNoOfQuestions)
    TextView mNoOfQuestions;

    Snackbar snackbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_info);
        ButterKnife.bind(this);

        mEndTest.setText("Start Test");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.selPaper);
        mEndTest.setVisibility(View.VISIBLE);
        mPageTitle.setText(paperBean.getSubjectName());
        mTimer.setText(getString(R.string.time_of_test, paperBean.getTime()));
        mNoOfQuestions.setText(getString(R.string.no_of_questions, paperBean.getTotalQuestion()));


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator animator = mChapterName.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }


        if (paperBean.getChapterList() != null) {
            mChapter.setVisibility(View.VISIBLE);
            ChapterAdapter chapterAdapter = new ChapterAdapter(makeGenres(paperBean.getChapterList()));
            mChapterName.setLayoutManager(layoutManager);
            mChapterName.setAdapter(chapterAdapter);
        } else
            mChapter.setVisibility(View.GONE);
    }

    public List<Genre> makeGenres(List<ChapterBean> chapterBeans) {
        List<Genre> genres = new ArrayList<>();
        for (int i = 0; i < chapterBeans.size(); i++) {
            genres.add(makeJazzGenre(chapterBeans.get(i)));
        }
        return genres;
    }

    public static Genre makeJazzGenre(ChapterBean chapterBean) {
        return new Genre(chapterBean.getChapterName(), makeJazzArtists(chapterBean.getSubChapter()), 0);
    }

    public static List<Artist> makeJazzArtists(List<ChapterBean> subChapter) {
        List<Artist> artists = new ArrayList<>();
        for (int i = 0; i < subChapter.size(); i++) {
            artists.add(new Artist(subChapter.get(i).getChapterName(), true));
        }
        return artists;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.mBackBtn, R.id.mEndTest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mEndTest:
                if (Utils.isNetworkAvailable(this, false)) {
                    if (paperBean.getType().equalsIgnoreCase("0"))
                        startActivity(new Intent(this, TodayTest.class).putExtra(Constant.selPaper, paperBean));
                    else
                        startActivity(new Intent(this, TodayTestVideo.class).putExtra(Constant.selPaper, paperBean));
                } else
                    callSnackBar();
                break;
        }
    }

    private void callSnackBar() {
        snackbar = Snackbar
                .make(mBackBtn, "Please check internet connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> {
                    if (Utils.isNetworkAvailable(this, false))
                        startActivity(new Intent(this, TodayTest.class).putExtra(Constant.selPaper, paperBean));
                    else
                        callSnackBar();
                });
        snackbar.show();
    }
}
