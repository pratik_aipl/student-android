package com.childrens.academy.listner;

import com.childrens.academy.bean.SubjectBean;

public interface SubjectOnClick {
    public void callBackSubject(int pos, SubjectBean subjectBean);
}
