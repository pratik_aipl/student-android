package com.childrens.academy.listner;

import com.childrens.academy.bean.LevelBean;

public interface LevelOnClick {
    public void callBackLevel(int pos, LevelBean subjectBean);
}
