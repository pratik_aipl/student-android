package com.childrens.academy.testsection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.TestDetails;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.xvideoplayer.MxMediaManager;
import com.childrens.academy.view.xvideoplayer.MxUserAction;
import com.childrens.academy.view.xvideoplayer.MxVideoPlayer;
import com.childrens.academy.view.xvideoplayer.MxVideoPlayerWidget;
import com.childrens.academy.view.xvideoplayer.PlayerPause;
import com.childrens.academy.view.xvideoplayer.UpdatePlayerData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_AUTO_COMPLETE;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_ERROR;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_NORMAL;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PAUSE;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING;
import static com.childrens.academy.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING_BUFFERING_START;


public class TestVideoView extends ActivityBase {

    private static final String TAG = "TestVideoView";
    Unbinder unbinder;
    TestDetails testDetails;

    int mCurrentScreen = CURRENT_STATE_NORMAL, mCurrentState = 0;
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mVideo)
    ImageView mVideo;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;
    @BindView(R.id.mVideoView1)
    MxVideoPlayerWidget mVideoView1;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;
    @BindView(R.id.forward)
    ImageButton forward;
    @BindView(R.id.play_pause_video1)
    ImageButton playPauseVideo1;
    @BindView(R.id.play_pause_video)
    ImageButton playPauseVideo;
    @BindView(R.id.play_time)
    TextView playTime;
    @BindView(R.id.mDurations)
    TextView mDurations;
    @BindView(R.id.mPointerView)
    RelativeLayout mPointerView;
    @BindView(R.id.mProgressVideo)
    SeekBar mProgressVideo;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        unSubScribeEvent(true);
        testDetails = (TestDetails) getIntent().getSerializableExtra(Constant.selPaper);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mPageTitle.setText("Video Assignment");

        Log.d(TAG, "onCreate: start "+testDetails.getStartTime() );
        Log.d(TAG, "onCreate: end "+testDetails.getEndTime() );
        Log.d(TAG, "onCreate: duration "+(testDetails.getEndTime() - testDetails.getStartTime()));
        Log.d(TAG, "onCreate: url "+testDetails.getVideoURL());
        mDurations.setText(Utils.stringForTime(testDetails.getEndTime() - testDetails.getStartTime()));
        mProgressVideo.setMax((int) (testDetails.getEndTime() - testDetails.getStartTime()));
        playTime.setText("00:00");
        mVideoView1.startPlay(testDetails.getVideoURL().replace("https", "http"), testDetails.getStartTime(), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, "");

        Utils.loadImageWithPicasso(this, testDetails.getVideoImage(), mVideoView1.mThumbImageView, mProgress);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerPauseEvent(PlayerPause event) {
        mCurrentScreen = event.getmCurrentScreen();
        mCurrentState = event.getState();
        Log.d(TAG, "onPlayerPauseEvent: " + event.getState());
        if (mCurrentState == CURRENT_STATE_NORMAL || mCurrentState == CURRENT_STATE_ERROR) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_PLAYING) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
            mVideoView1.mThumbImageView.setVisibility(View.GONE);

        } else if (mCurrentState == CURRENT_STATE_PLAYING_BUFFERING_START) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
            mVideoView1.mThumbImageView.setVisibility(View.GONE);

        } else if (mCurrentState == CURRENT_STATE_PAUSE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_AUTO_COMPLETE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdatePlayerDataEvent(UpdatePlayerData event) {
        playTime.setText(Utils.stringForTime((event.getCurrentTimeP() - testDetails.getStartTime())));
        mProgressVideo.setProgress((int) (event.getCurrentTimeP() - testDetails.getStartTime()));

        Log.d(TAG, "onUpdatePlayerDataEvent: " + testDetails.getEndTime());
        Log.d(TAG, "onUpdatePlayerDataEvent: " + event.getCurrentTimeP());

        if (event.getCurrentTimeP() >= testDetails.getEndTime()) {
            playTime.setText("00:00");
            mProgressVideo.setProgress(0);
            mVideoView1.obtainCache();
            mVideoView1.onActionEvent(MxUserAction.ON_CLICK_PAUSE);
            mVideoView1.mThumbImageView.setVisibility(View.VISIBLE);
            MxMediaManager.getInstance().getPlayer().pause();
            MxMediaManager.getInstance().getPlayer().seekTo(testDetails.getStartTime());
            mVideoView1.setUiPlayState(CURRENT_STATE_PAUSE);
        }
    }


    private void unSubScribeEvent(boolean isSubScribe) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        } else {
            EventBus.getDefault().unregister(this);
        }
    }

    @OnClick({R.id.mBackBtn, R.id.play_pause_video})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.play_pause_video:

                if (mCurrentState == 0) {
                    mVideoView1.mThumbImageView.performClick();
                    mVideoView1.mThumbImageView.setVisibility(View.GONE);
                } else {
                    mVideoView1.mPlayControllerButton.performClick();
                }
                break;

        }
    }

    @Override
    public void onBackPressed() {
        MxMediaManager.getInstance().releaseMediaPlayer();
        finish();


    }

    @Override
    protected void onDestroy() {
        unSubScribeEvent(true);
        super.onDestroy();
    }
}
