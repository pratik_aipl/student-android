package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.AnswerBean;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.OptionsWebView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pratik on 04/12/18.
 */
public class SummaryOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OptionAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<OptionBean> optionBeans;
    AnswerBean answerBean;
    String page;

    public SummaryOptionAdapter(Context context, AnswerBean answerBean, List<OptionBean> optionBeans, String page) {
        this.context = context;
        this.optionBeans = optionBeans;
        this.answerBean = answerBean;
        this.page = page;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_question_option_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        OptionBean beanData = optionBeans.get(position);
        holder.mOptionsText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mOptionsText.setScrollbarFadingEnabled(false);
        holder.mOptionsText.setInitialScale(30);
        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions());

        holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions());
        holder.mRadioBtn.setText(Utils.getOptionText(position));

        if (page.equalsIgnoreCase(Constant.incorrect)) {

            if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                holder.mRadioBtn.setChecked(true);
                if (position == 0)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round_right));
                else if (position == optionBeans.size() - 1)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round_right));
                else
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round_right));
            } else {
                if (answerBean.getAnswerID() == beanData.getMCQOptionID()) {
                    if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                        if (position == 0)
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round_right));
                        else if (position == optionBeans.size() - 1)
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round_right));
                        else
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round_right));

                    } else {
                        holder.mRadioBtn.setChecked(true);
                        if (position == 0)
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_ans_sel));
                        else if (position == optionBeans.size() - 1)
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_ans_sel));
                        else
                            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round_ans_sel));
                    }
                } else {
                    holder.mRadioBtn.setChecked(false);
                    if (position == 0)
                        holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
                    else if (position == optionBeans.size() - 1)
                        holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
                    else
                        holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
                }

            }

        } else {
            if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                if (position == 0)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round_right));
                else if (position == optionBeans.size() - 1)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round_right));
                else
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round_right));
            } else {
                holder.mRadioBtn.setChecked(false);
                if (position == 0)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
                else if (position == optionBeans.size() - 1)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
                else
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
            }
        }


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return optionBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        public LinearLayout mViewContainer;
        @BindView(R.id.mOptionsText)
        public OptionsWebView mOptionsText;
        @BindView(R.id.mRadioBtn)
        public RadioButton mRadioBtn;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
