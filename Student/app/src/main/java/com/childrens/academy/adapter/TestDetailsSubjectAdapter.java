package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.childrens.academy.R;
import com.childrens.academy.Report;
import com.childrens.academy.SubjectReport;
import com.childrens.academy.bean.SearchBean;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.listner.SearchOnClick;
import com.childrens.academy.listner.TestDetailsSubjectOnClick;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class TestDetailsSubjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<SubjectBean> paperBeanList;
    TestDetailsSubjectOnClick testOnClick;


    public TestDetailsSubjectAdapter(Report context, List<SubjectBean> paperBeanList) {
        this.context = context;
        this.paperBeanList = paperBeanList;
        this.testOnClick = context;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_test_details_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        SubjectBean paperBean = paperBeanList.get(position);
       /* if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else */if (position == paperBeanList.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mViewContainer.setOnClickListener(v -> {
            if (paperBean.getOverallPerformance()>0) {
                testOnClick.callbackPaper(position, paperBeanList.get(position));
            }else {
                Toast.makeText(context, "Assignments Not Available.", Toast.LENGTH_SHORT).show();
            }
        });
//        holder.mTotalAns.setBackground(ContextCompat.getDrawable(context,R.drawable.half_circle_bottom_green));
//        holder.mTotalAns.setTextColor(ContextCompat.getColor(context,R.color.white));
        holder.mAcView.setVisibility(View.VISIBLE);
        holder.mSubjectName.setText(""+paperBean.getSubjectName());

        holder.mRightAns.setText(context.getString(R.string.str_status_20,"",paperBean.getTotalMissed()));
        holder.mTotalAns.setText(context.getString(R.string.str_status_20,"",paperBean.getTotalAttempt()));
        holder.mEndDate.setVisibility(View.GONE);
//        holder.mAccuracyLabel.setText(context.getString(R.string.str_status_20,"Accuracy ",paperBean.getOverallPerformance())+"%");
        holder.mAccuracyLabel.setText("Accuracy "+paperBean.getOverallPerformance()+"%");
        holder.mAccuracy.setProgress(paperBean.getOverallPerformance());
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeanList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mRightAns)
        TextView mRightAns;
        @BindView(R.id.mTotalAns)
        TextView mTotalAns;
        @BindView(R.id.mSubjectName)
        TextView mSubjectName;
        @BindView(R.id.mEndDate)
        TextView mEndDate;
        @BindView(R.id.mAccuracy)
        ProgressBar mAccuracy;
        @BindView(R.id.mAccuracyLabel)
        TextView mAccuracyLabel;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;
        @BindView(R.id.mAcView)
        LinearLayout mAcView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
