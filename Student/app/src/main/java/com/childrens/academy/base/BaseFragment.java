/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.childrens.academy.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;


import com.childrens.academy.R;
import com.childrens.academy.bean.UserData;
import com.childrens.academy.network.RestAPIBuilder;
import com.childrens.academy.network.RestApi;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Prefs;
import com.google.gson.Gson;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;

public class BaseFragment extends Fragment {
    protected LinearLayoutManager layoutManager;
    private static final String TAG = "BaseFragment";

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected RxPermissions rxPermissions;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();

        Gson gson = new Gson();
        prefs = Prefs.with(getActivity());
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);

    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            if (!progressDialog.isShowing())
                progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
