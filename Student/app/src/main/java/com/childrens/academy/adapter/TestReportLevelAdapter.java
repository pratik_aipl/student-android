package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.LevelBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class TestReportLevelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelBean> testBeans;
    int totalQuestion;

    // Provide a suitable constructor (depends on the kind of dataset)
//    public SearchTestAdapter(Context context, List<LikeCommentData> likeBeanList) {
//        this.context = context;
//        this.likeBeanList = likeBeanList;
//    }
    public TestReportLevelAdapter(Context context, List<LevelBean> testBeans, int totalQuestion) {
        this.context = context;
        this.testBeans = testBeans;
        this.totalQuestion = totalQuestion;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_level_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        LevelBean levelBean = testBeans.get(position);

        holder.mLevelName.setText(levelBean.getLevel());
        holder.mLevelValue.setText(levelBean.getCount() + " - " + totalQuestion);

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return testBeans.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mLevelProgress)
        ProgressBar mLevelProgress;
        @BindView(R.id.mLevelValue)
        TextView mLevelValue;
        @BindView(R.id.mLevelName)
        TextView mLevelName;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
