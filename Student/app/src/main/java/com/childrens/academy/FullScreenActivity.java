package com.childrens.academy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.senab.photoview.PhotoView;

public class FullScreenActivity extends ActivityBase {

    private static final String TAG = "FullScreenActivity";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;
    @BindView(R.id.mImage)
    PhotoView mImage;

    String imagePath;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        ButterKnife.bind(this);
        imagePath = getIntent().getStringExtra(Constant.imagePath);
        mPageTitle.setText("Image");
        mImage.setVisibility(View.VISIBLE);
        Utils.loadImageWithPicasso(this, imagePath, mImage, mProgress);


    }

    @OnClick(R.id.mBackBtn)
    public void onViewClicked() {
        onBackPressed();
    }
}
