package com.childrens.academy.listner;


import com.childrens.academy.bean.OptionBean;

public interface OptionsOnClick {
    public void callbackOption(int pos, OptionBean paperBean);
}
