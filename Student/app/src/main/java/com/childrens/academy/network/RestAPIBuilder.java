package com.childrens.academy.network;


import android.util.Log;

import com.childrens.academy.Apps;
import com.childrens.academy.BuildConfig;
import com.childrens.academy.listner.InternetConnetionLostEvent;
import com.childrens.academy.listner.PageReloadEvent;
import com.childrens.academy.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by Pratik on 03/12/16
 */

public class RestAPIBuilder {

    private static final String TAG = "RestAPIBuilder";

    public static RestApi buildRetrofitService() {
        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        OkHttpClient.Builder builder = okHttpClient.newBuilder();
        builder.readTimeout(2, TimeUnit.MINUTES);
        builder.connectTimeout(2, TimeUnit.MINUTES);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

//        builder.addInterceptor(new NetworkConnectionInterceptor() {
//            @Override
//            public boolean isInternetAvailable() {
//
//                return true;
//            }
//
//            @Override
//            public void onInternetUnavailable() {
//
//            }
//
//            @Override
//            public void onCacheUnavailable() {
//
//            }
//        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) {
                Request request;
                String versionName = BuildConfig.VERSION_NAME;
                if (Utils.isLogin(Apps.mContext)) {
                    Log.d(TAG, "buildRetrofitService: USER-ID " + Utils.getUser(Apps.mContext).getId());
                    Log.d(TAG, "buildRetrofitService: LOGIN-TOKEN " + Utils.getAuthtoken(Apps.mContext));
                    request = chain.request().newBuilder()
                            .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                            .addHeader("X-CI-CHILDRENS-ACADEMY-API-KEY", BuildConfig.apiKry)
                            .addHeader("X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN", Utils.getAuthtoken(Apps.mContext))
                            .addHeader("User-Id", Utils.getUser(Apps.mContext).getId())
                            .build();
                } else {
                    request = chain.request().newBuilder()
                            .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                            .addHeader("X-CI-CHILDRENS-ACADEMY-API-KEY", BuildConfig.apiKry)
                            .build();
                }

                try {
                    return chain.proceed(request);
                } catch (IOException e) {
                    EventBus.getDefault().post(new InternetConnetionLostEvent(true));
                }
                return null;
            }
        });


        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.API_URL)
                .client(client).addConverterFactory(new ToStringConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();

        return retrofit.create(RestApi.class);
    }

}
