package com.childrens.academy.view.expandablerecyclerview.listeners;

public interface OnChildrenCheckStateChangedListener {

  /**
   * @param firstChildFlattenedIndex the flat position of the first child in the {@link
   * com.childrens.academy.view.expandablerecyclerview.models.CheckedExpandableGroup}
   * @param numChildren the total number of children in the {@link com.childrens.academy.view.expandablerecyclerview.models.CheckedExpandableGroup}
   */
  void updateChildrenCheckState(int firstChildFlattenedIndex, int numChildren);
}
